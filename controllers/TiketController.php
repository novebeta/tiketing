<?php
namespace app\controllers;
use app\models\Tiket;
use app\models\TiketSearch;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii2tech\csvgrid\CsvGrid;
/**
 * TiketController implements the CRUD actions for Tiket model.
 */
class TiketController extends Controller {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'rules' => [
					[
						'actions' => [ 'error' ],
						'allow'   => true,
					],
					[
						'actions' => [ 'index', 'update', 'delete', 'export' ],
						'allow'   => true,
						'roles'   => [ '@' ],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'delete' => [ 'POST' ],
				],
			],
		];
	}
	public function actions() {
		$this->layout = 'backend';
	}
	/**
	 * Lists all Tiket models.
	 * @return mixed
	 */
	public function actionIndex() {
		$this->layout = 'backend';
		$searchModel  = new TiketSearch();
		//$_GET['TiketSearch']['expired'] = date('Y-m-d H:i:s');
		$param        = Yii::$app->request->queryParams;
		$dataProvider = $searchModel->search( $param );
		return $this->render( 'index', [
			'searchModel'  => $searchModel,
			'dataProvider' => $dataProvider,
		] );
	}

	public function actionExport()
    {
        $exporter = new CsvGrid([
            'dataProvider' => new ActiveDataProvider([
                'query' => Tiket::find(), // over 1 million records
            ]),
            // 'maxEntriesPerFile' => 60000,
        ]);

        return $exporter->export()->send('tiket.csv'); // displays dialog for saving `items.csv.zip`!
    }

	/**
	 * Displays a single Tiket model.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionView( $id ) {
		return $this->render( 'view', [
			'model' => $this->findModel( $id ),
		] );
	}
	/**
	 * Creates a new Tiket model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 * @return mixed
	 */
	public function actionCreate() {
		$model = new Tiket();
		if ( $model->load( Yii::$app->request->post() ) && $model->save() ) {
			return $this->redirect( [ 'view', 'id' => $model->tiket_id ] );
		}
		return $this->render( 'create', [
			'model' => $model,
		] );
	}
	/**
	 * Updates an existing Tiket model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionUpdate( $id ) {
		$model = $this->findModel( $id );
		if ( $model->load( Yii::$app->request->post() ) ) {
			if ( $model->transaksi_tiket == '' ) {
				$model->addError( 'transaksi_tiket', 'No Tiket harus diisi untuk di email ke pengunjung.' );
			}
			if ( $model->transaksi_bank == '' ) {
				$model->addError( 'transaksi_bank', 'Bank penerima transfer harus jelas.' );
			}
			if ( $model->transaksi_no == '' ) {
				$model->addError( 'transaksi_no', 'No transaksi transfer harus jelas.' );
			}
			if ( $model->transaksi_pengirim == '' ) {
				$model->addError( 'transaksi_pengirim', 'Nama pengirim transfer harus jelas.' );
			}
			if ( $model->no_invoice == '' ) {
				$model->no_invoice = Tiket::genInv( $model->jenis_tur );
			}
			$model->transaksi_waktu = date( 'Y-m-d H:i:s' );
			if ( ! $model->hasErrors() && $model->save() ) {
				Yii::$app->mailer->compose( 'tiket', [
					'completeName' => $model->nama_lengkap,
					'bank'         => $model->transaksi_bank,
					'tiket'        => $model->transaksi_tiket,
					'no_booking'   => $model->no_booking,
					'no_invoice'   => $model->no_invoice,
					'tourSelect'   => $model->jenis_tur,
					'ticketDate'   => date( 'd/m/Y', strtotime( $model->tiket_tgl ) ),
					'ticketTime'   => $model->tiket_jam,
					'qtyDewasa'    => $model->dewasa,
					'qtyAnak'      => $model->anak,
				] )
				                 ->setFrom( 'tickets@ullensentalu.com' )
				                 ->setTo( $model->email )
				                 ->setSubject( "Tiket Kunjungan Ullen Sentalu Tanggal " .
				                               date( 'd/m/Y', strtotime( $model->tiket_tgl ) ) ) // тема письма
//			                 ->setHtmlBody(print_r($_SESSION,true))
				                 ->send();
				return $this->redirect( [ 'tiket/index' ] );
			} else {
//				Yii::$app->getSession()->setFlash( 'error', Html::errorSummary( $model ) );
				return $this->render( 'update', [
					'model' => $model,
				] );
			}
		}
		return $this->render( 'update', [
			'model' => $model,
		] );
	}
	/**
	 * Deletes an existing Tiket model.
	 * If deletion is successful, the browser will be redirected to the 'index' page.
	 *
	 * @param string $id
	 *
	 * @return mixed
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	public function actionDelete( $id ) {
		$this->findModel( $id )->delete();
		return $this->redirect( [ 'index' ] );
	}
	/**
	 * Finds the Tiket model based on its primary key value.
	 * If the model is not found, a 404 HTTP exception will be thrown.
	 *
	 * @param string $id
	 *
	 * @return Tiket the loaded model
	 * @throws NotFoundHttpException if the model cannot be found
	 */
	protected function findModel( $id ) {
		if ( ( $model = Tiket::findOne( $id ) ) !== null ) {
			return $model;
		}
		throw new NotFoundHttpException( 'The requested page does not exist.' );
	}
}
