<?php
namespace app\controllers;
use app\models\ContactForm;
use app\models\LoginForm;
use app\models\Tiket;
use Yii;
use yii\db\Expression;
use yii\filters\AccessControl;
use yii\filters\VerbFilter;
use yii\web\Controller;
use yii\web\Response;
class SiteController extends Controller {
	/**
	 * {@inheritdoc}
	 */
	public function behaviors() {
		return [
			'access' => [
				'class' => AccessControl::className(),
				'only'  => [ 'logout' ],
				'rules' => [
					[
						'actions' => [ 'logout' ],
						'allow'   => true,
						'roles'   => [ '@' ],
					],
				],
			],
			'verbs'  => [
				'class'   => VerbFilter::className(),
				'actions' => [
					'logout' => [ 'post' ],
				],
			],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function actions() {
		return [
			'error'   => [
				'class' => 'yii\web\ErrorAction',
			],
			'captcha' => [
				'class'           => 'yii\captcha\CaptchaAction',
				'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
			],
		];
	}
	/**
	 * Displays homepage.
	 *
	 * @return string
	 */
	public function actionIndex() {
		$session = Yii::$app->session;
		$session->open();
		if ( isset( $_GET[ 'oper' ] ) && $_GET[ 'oper' ] == 'flush' ) {
			$session->destroy();
		}
		if ( Yii::$app->request->isPost ) {
			$session->set( 'tourSelect', $_POST[ 'tour-select' ] );
		}
		$tourSelect    = $session->has( 'tourSelect' ) ? $session->get( 'tourSelect' ) : 'Adiluhung Mataram';
		$session->set( 'tourSelect', $tourSelect);
		$peopleSelect  = $session->has( 'peopleSelect' ) ? $session->get( 'peopleSelect' ) : 1;
		$ticketDate    = $session->has( 'ticketDate' ) ? $session->get( 'ticketDate' ) : date( 'Y-m-d' );
		$ticketTime    = $session->has( 'ticketTime' ) ? $session->get( 'ticketTime' ) : date( 'H:i' );
		$ticketDateMin = date( 'Y-m-d', strtotime( '+1 days' ) );
		if ( isset( $_GET[ 'mode' ] ) && $_GET[ 'mode' ] == 'admin' ) {
			$ticketDateMin = date( 'Y-m-d' );
		}
		$ticketDateMax = date( 'Y-m-d', strtotime( '+60 days' ) );
		$disabledDays  = Yii::$app->params[ 'disabledDays' ];
		$openDays      = Yii::$app->params[ 'openDays' ];
		$disabledDates = Yii::$app->params[ 'disabledDates' ];
		if (( isset( $_GET[ 'mode' ] ) && $_GET[ 'mode' ] == 'admin') || Yii::$app->params[ 'allowOnDay' ]  ) {
			$ticketDateMin = date( 'Y-m-d' );
		}
		$soldoutDays = Tiket::find()->getDateSoldOut( $tourSelect, $ticketDateMin, $ticketDateMax );
		$session->close();
		return $this->render( 'index', [
			'tourSelect'    => $tourSelect,
			'peopleSelect'  => $peopleSelect,
			'ticketDate'    => $ticketDate,
			'ticketTime'    => $ticketTime,
			'ticketDateMin' => $ticketDateMin,
			'ticketDateMax' => $ticketDateMax,
			'soldoutDays'   => $soldoutDays,
			'disabledDays'  => $disabledDays,
			'disabledDates' => $disabledDates,
			'openDays'      => $openDays,
		] );
	}
	public function actionTiket() {
		$session = Yii::$app->session;
		$session->open();
		if ( Yii::$app->request->isPost ) {
			$session->set( 'tourSelect', $_POST[ 'tour-select' ] );
			$session->set( 'peopleSelect', $_POST[ 'people-select' ] );
			$session->set( 'ticketDate', $_POST[ 'ticketDate' ] );
			$session->set( 'ticketTime', $_POST[ 'ticketTime' ] );
		} else {
			if ( ! $session->has( 'tourSelect' ) ) {
				return $this->redirect( [ 'site/index' ] );
			}
		}
		$tourSelect   = $session->get( 'tourSelect' );
		$peopleSelect = $session->get( 'peopleSelect' );
		$ticketDate   = date( 'd/m/Y', strtotime( $session->get( 'ticketDate' ) ) );
		$ticketTime   = $session->get( 'ticketTime' );
		$hargaDewasa  = Yii::$app->params[ $tourSelect ][ 'adult' ];
		$hargaAnak    = Yii::$app->params[ $tourSelect ][ 'child' ];
		$session->close();
		$sisa = Tiket::find()->getSisaTimeSlot( $tourSelect, $session->get( 'ticketDate' ), $ticketTime );
		if ( in_array( $tourSelect, Yii::$app->params[ 'disabledTour' ] ) ) {
			$sisa = 0;
		}
		return $this->render( 'tiket', [
			'sisa'         => $sisa,
			'tourSelect'   => $tourSelect,
			'peopleSelect' => $peopleSelect,
			'ticketDate'   => $ticketDate,
			'ticketTime'   => $ticketTime,
			'hargaDewasa'  => $hargaDewasa,
			'hargaAnak'    => $hargaAnak,
		] );
	}
	private function calculate( $tourSelect, $qtyDewasa, $qtyAnak ) {
		$hargaDewasa = Yii::$app->params[ $tourSelect ][ 'adult' ];
		$hargaAnak   = Yii::$app->params[ $tourSelect ][ 'child' ];
		$totDewasa   = $qtyDewasa * $hargaDewasa;
		$totAnak     = $qtyAnak * $hargaAnak;
		$total       = $totDewasa + $totAnak;
		return [
			'totDewasa' => $totDewasa,
			'totAnak'   => $totAnak,
			'total'     => $total
		];
	}
	public function actionCheckout() {
		$session = Yii::$app->session;
		$session->open();
		if ( Yii::$app->request->isPost ) {
			$expiredTrial   = date( 'Y-m-d H:i:s', strtotime( Yii::$app->params[ 'durasiExpired' ] ) );
			$expired        = $expiredTrial;
			$maxExpired     = date( 'Y-m-d ' . Yii::$app->params[ 'maxTimeExpired' ] );
			$dateTimestamp1 = strtotime( $maxExpired );
			$dateTimestamp2 = strtotime( $expiredTrial );
			if ( $dateTimestamp2 > $dateTimestamp1 ) {
				$expired = date( 'Y-m-d', strtotime( ' +1 day' ) ) . ' ' . Yii::$app->params[ 'nextDayTimeExpired' ];
			}
			$session->set( 'qtyDewasa', $_POST[ 'qtyDewasa' ] );
			$session->set( 'qtyAnak', $_POST[ 'qtyAnak' ] );
			$session->set( 'completeName', $_POST[ 'completeName' ] );
			$session->set( 'handphone', $_POST[ 'handphone' ] );
			$session->set( 'emailAddress', $_POST[ 'emailAddress' ] );
			$session->set( 'city', $_POST[ 'city' ] );
			$session->set( 'country', $_POST[ 'country' ] );
			$session->set( 'expired', $expired );
//			$uuid = \thamtech\uuid\helpers\UuidHelper::uuid();
//			$session->set( 'tiket_id', $uuid );
			$model             = new Tiket;
			$model->tiket_id   = ( new \yii\db\Query )->select( new Expression( "uuid()" ) )->scalar();
			$model->no_booking = ( new \yii\db\Query )->select( new Expression( "UPPER(SUBSTRING(MD5(RAND()) FROM (FLOOR(RAND()*(23-1)+1)) FOR 10))" ) )->scalar();
			$model->tdate      = date( 'Y-m-d H:i:s' );
			$tourSelect        = $session->get( 'tourSelect' );
			if ( $tourSelect == '' ) {
				$this->redirect( [ 'site/index' ] );
				Yii::$app->end( 0 );
			}
			$ticketDate          = $session->get( 'ticketDate' );
			$ticketTime          = $session->get( 'ticketTime' );
			$hargaDewasa         = Yii::$app->params[ $tourSelect ][ 'adult' ];
			$hargaAnak           = Yii::$app->params[ $tourSelect ][ 'child' ];
			$qtyDewasa           = $session->get( 'qtyDewasa' );
			$qtyAnak             = $session->get( 'qtyAnak' );
			$peopleSelect        = ceil( ( $qtyDewasa + $qtyAnak ) / intval( Yii::$app->params[ 'timeslot' ][ 'perSlot' ] ) );
			$completeName        = $session->get( 'completeName' );
			$handphone           = $session->get( 'handphone' );
			$emailAddress        = $session->get( 'emailAddress' );
			$expired             = $session->get( 'expired' );
			$calc                = self::calculate( $tourSelect, $qtyDewasa, $qtyAnak );
			$model->jenis_tur    = $tourSelect;
			$model->jml          = $peopleSelect;
			$model->tiket_tgl    = $ticketDate;
			$model->tiket_jam    = $ticketTime;
			$model->dewasa       = $qtyDewasa;
			$model->anak         = $qtyAnak;
			$model->dewasa_harga = $hargaDewasa;
			$model->anak_harga   = $hargaAnak;
			$model->total        = $calc[ 'total' ];
			$model->nama_lengkap = $completeName;
			$model->no_hp        = $handphone;
			$model->email        = $emailAddress;
			$model->kota         = $session->get( 'city' );
			$model->negara       = $session->get( 'country' );
			$model->expired      = $expired;
			$model->save();
			$model->refresh();
			$valSlot = Tiket::find()->getSisaTimeSlot( $tourSelect, $session->get( 'ticketDate' ), $ticketTime );
			if ( $valSlot < 0 ) {
				Yii::$app->getSession()->setFlash( 'error', "Maaf, tiket sudah habis.. anda kurang cepat" );
				$model->delete();
				return $this->redirect( [ 'site/index' ] );
			}
			Yii::$app->mailer->compose( 'booking', [
				'no_booking'   => $model->no_booking,
				'tourSelect'   => $tourSelect,
				'peopleSelect' => $peopleSelect,
				'ticketDate'   => date( 'd/m/Y', strtotime( $ticketDate ) ),
				'ticketTime'   => $ticketTime,
				'hargaDewasa'  => $hargaDewasa,
				'hargaAnak'    => $hargaAnak,
				'qtyDewasa'    => $qtyDewasa,
				'qtyAnak'      => $qtyAnak,
				'totDewasa'    => $calc[ 'totDewasa' ],
				'totAnak'      => $calc[ 'totAnak' ],
				'total'        => $calc[ 'total' ],
				'completeName' => $completeName,
				'handphone'    => $handphone,
				'emailAddress' => $emailAddress,
				'expired'      => date( 'd/m/Y H:i:s', strtotime( $expired ) )
			] )
			                 ->setFrom( 'tickets@ullensentalu.com' )
			                 ->setTo( $emailAddress )
			                 ->setSubject( "Menunggu Pembayaran Reservasi Ullen Sentalu No {$model->no_booking}" ) // тема письма
//			                 ->setHtmlBody(print_r($_SESSION,true))
			                 ->send();
			$session->destroy();
			$session->close();
			return $this->render( 'checkout', [
				'no_booking'   => $model->no_booking,
				'tourSelect'   => $tourSelect,
				'peopleSelect' => $peopleSelect,
				'ticketDate'   => date( 'd/m/Y', strtotime( $ticketDate ) ),
				'ticketTime'   => $ticketTime,
				'hargaDewasa'  => $hargaDewasa,
				'hargaAnak'    => $hargaAnak,
				'qtyDewasa'    => $qtyDewasa,
				'qtyAnak'      => $qtyAnak,
				'totDewasa'    => $calc[ 'totDewasa' ],
				'totAnak'      => $calc[ 'totAnak' ],
				'total'        => $calc[ 'total' ],
				'completeName' => $completeName,
				'handphone'    => $handphone,
				'emailAddress' => $emailAddress,
				'expired'      => date( 'd/m/Y H:i:s', strtotime( $expired ) )
			] );
		}
		return $this->redirect( [ 'site/index' ] );
	}
//	public function actionCheckYourOrder() {
//		if ( Yii::$app->request->isPost ) {
//			$session = Yii::$app->session;
//			$session->open();
//			$session[ 'page_3' ] = [
//				"firstName"           => $_POST[ 'firstName' ],
//				"surName"             => $_POST[ 'surName' ],
//				"handphone"           => $_POST[ 'handphone' ],
//				"emailAddress"        => $_POST[ 'emailAddress' ],
//				"emailAddressConfirm" => $_POST[ 'emailAddressConfirm' ],
//				"city"                => $_POST[ 'city' ],
//				"country"             => $_POST[ 'country' ],
//				"passbook"            => $_POST[ 'passbook' ],
//				"newsletter"          => $_POST[ 'newsletter' ],
//			];
//			$session->close();
//		}
//		return $this->render( 'check-your-order' );
//	}
	/**
	 * Login action.
	 *
	 * @return array
	 */
	public function actionTimeslot() {
		\Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
		$times                       = Tiket::hoursRange( Yii::$app->params[ 'timeslot' ][ 'lower' ] * 3600,
			Yii::$app->params[ 'timeslot' ][ 'upper' ] * 3600, Yii::$app->params[ 'timeslot' ][ 'step' ] * 60, 'H:i' );
		$session                     = Yii::$app->session;
		$session->open();
		$tourSelect = $session->get( 'tourSelect' );
		$pick       = Tiket::find()->getTimeSlot( $tourSelect, $_POST[ 'ticketDate' ] );
		$max        = Yii::$app->params[ 'timeslot' ][ 'max' ];
		$timeslots  = [];
		foreach ( $times as $k => $v ) {
			$available = true;
			$busy      = false;
			if ( isset( $pick[ $v ] ) && $pick[ $v ] >= $max ) {
				$available = false;
			}
			if ( isset( $pick[ $v ] ) && $pick[ $v ] < $max ) {
				$busy = true;
			}
			$timeslots[ $v ] = [
				'available' => ! in_array( $v, Yii::$app->params[ 'timeslot' ][ 'disabled' ] ) && $available,
				'busy'      => in_array( $v, Yii::$app->params[ 'timeslot' ][ 'busy' ] ) || $busy
			];
		}
		return [
			'success'   => true,
			'error'     => false,
			'timeslots' => $timeslots
		];
	}
	public function actionLogin() {
		if ( ! Yii::$app->user->isGuest ) {
			return $this->goHome();
		}
		$model = new LoginForm();
		if ( $model->load( Yii::$app->request->post() ) && $model->login() ) {
			return $this->goBack();
		}
		$model->password = '';
		return $this->render( 'login', [
			'model' => $model,
		] );
	}
	/**
	 * Logout action.
	 *
	 * @return Response
	 */
	public function actionLogout() {
		Yii::$app->user->logout();
		return $this->goHome();
	}
	/**
	 * Displays contact page.
	 *
	 * @return Response|string
	 */
	public function actionContact() {
		$model = new ContactForm();
		if ( $model->load( Yii::$app->request->post() ) && $model->contact( Yii::$app->params[ 'adminEmail' ] ) ) {
			Yii::$app->session->setFlash( 'contactFormSubmitted' );
			return $this->refresh();
		}
		return $this->render( 'contact', [
			'model' => $model,
		] );
	}
	/**
	 * Displays about page.
	 *
	 * @return string
	 */
	public function actionAbout() {
		return $this->render( 'about' );
	}
}
