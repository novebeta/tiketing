/* number_format */
function number_format(a, b, c, d) {
    a = (a + "").replace(/[^0-9+-Ee.]/g, "");
    var e = !isFinite(+a) ? 0 : +a
        , f = !isFinite(+b) ? 0 : Math.abs(b)
        , g = typeof d === "undefined" ? "," : d
        , h = typeof c === "undefined" ? "." : c
        , i = ""
        , j = function (a, b) {
        var c = Math.pow(10, b);
        return "" + Math.round(a * c) / c
    };
    i = (f ? j(e, f) : "" + Math.round(e)).split(".");
    if (i[0].length > 3) {
        i[0] = i[0].replace(/B(?=(?:d{3})+(?!d))/g, g)
    }
    if ((i[1] || "").length < f) {
        i[1] = i[1] || "";
        i[1] += (new Array(f - i[1].length + 1)).join("0")
    }
    return i.join(h)
}

function number2string(bilangan) {
    var number_string = bilangan.toString(),
        sisa = number_string.length % 3,
        rupiah = number_string.substr(0, sisa),
        ribuan = number_string.substr(sisa).match(/\d{3}/g);
    if (ribuan) {
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }
    return rupiah;
}

$('ol#prosedur li').click(function () {
    var id = "#" + $(this).attr('value');
    $(".prosedurDesc").hide();
    $(id).fadeIn("slow");
});
$('#tour-select').change(function () {
    $('#gt').attr('action','');
    $('#gt').submit();
    // switch (this.value) {
    //     case 'Adiluhung Mataram':
    //         $('#descriptionTour').html('Area Guwo Selo Giri dan Kampung Kambang');
    //         break;
    //     case 'Vorstenlanden':
    //         $('#descriptionTour').html('Area Jagad Galeri, Esther Huis, Sasono Sekar Bawono ... Menyusul .. akan dibuka segera ..');
    //         break;
    // }
});
/*
* variable
*/
var effectSpeed = 'slow';
var ajaxRequest;
$('.ticket-info').find('.ticket-info__list').find('li:nth-child(4)').hide();

/*
* date functions
*/
function getDisabledDates(date) {
    var month = ("0" + (date.getMonth() + 1)).slice(-2);
    var d = date.getFullYear() + '-' + month + '-' + ("0" + date.getDate()).slice(-2);
    if (jQuery.inArray(d, soldoutDays) >= 0) {
        return [false, 'soldout'];
    } else if (jQuery.inArray(date.getDay(), disabledDays) >= 0 || jQuery.inArray(d, disabledDates) >= 0) {
        if (jQuery.inArray(d, openDays) < 0) {
            return [false, 'disabled'];
        }
    }
    return [true];
}

$.datepicker.setDefaults($.datepicker.regional[language]);
$('#datepicker').datepicker({
    altField: '#ticketDate',
    altFormat: 'yy-mm-dd',
    dateFormat: 'yy-mm-dd',
    minDate: ticketDateMin,
    maxDate: ticketDateMax,
    beforeShowDay: getDisabledDates,
    firstDay: 1,
    numberOfMonths: 2,
    onSelect: function (dateText, inst) {
        /* ticketDate variable */
        var ticketDate = $(this).val();
        //
        // /* summer text */
        $('#zomer-vrijdag').hide();
        $('#zomer-zaterdag').hide();
        //
        // /* load timeslots */
        loadTimeslots(ticketDate);
    }
});

function datepickerDisplay() {
    var winWidth = $(window).width();
    $('#datepicker').datepicker('option', 'numberOfMonths', 2);
    if (winWidth < 768) {
        $('#datepicker').datepicker('option', 'numberOfMonths', 1);
    }
}

datepickerDisplay();
$(window).resize(function () {
    datepickerDisplay();
});
/* remove default datepicker date */
$('.ui-datepicker-current-day').removeClass('ui-datepicker-current-day');
/*
$('#ticketDate').change(function()
{
    loadTimeslots($(this).val());

    return false;
});
*/
if ($('.form-persons').length > 0 && $('.form-persons').find('.row-ticketType:hidden').length == 0) {
    $('.ticket-info__more-options').hide();
}
var firstLoad = false;

/* loadTimeslots */
function loadTimeslots(ticketDate) {
    /* loaded */
    var loaded = false;
    /* hide timeslots */
    $('.time-selection, .time-selection .timeslots, .timeslots-waanzin').hide();
    /* deactive button */
    $('div.ticket-button, div.ticket-button-submit').removeClass('active');
    /* abort previous ajax request */
    if (ajaxRequest) {
        ajaxRequest.abort();
    }
    var vgjStart = new Date('2018-03-23').getTime();
    var vgjEnd = new Date('2018-06-24').getTime();
    var ticketDateCheck = new Date(ticketDate).getTime();
    if (ticketDateCheck >= vgjStart && ticketDateCheck <= vgjEnd) {
        $('header.header').addClass('van-gogh-in-japan');
    } else if ($('header.header').hasClass('van-gogh-in-japan')) {
        $('header.header').removeClass('van-gogh-in-japan');
    }
    var numberOfTickets = $('#people-select').val();
    /* load timeslots */
    ajaxRequest = $.ajax({
        url: 'index.php?r=site/timeslot',
        cache: false,
        dataType: 'json',
        data: {
            _csrf: $('meta[name="csrf-token"]').attr("content"),
            ticketDate: ticketDate,
            language: $('html').attr('lang'),
            numberOfTickets: numberOfTickets
        },
        beforeSend: function () {
            showLoading();
        },
        method: 'POST',
        success: function (output) {
            /* clear loading */
            loaded = true;
            /* clear table-time */
            $('.timeslots .table-time').html('');
            $('.timeslots .table-time-overflow').html('').hide();
            $('.timeslots .overflow-container').hide();
            /* success */
            if (output.success) {
                if (output.timeslots) {
                    $.each(output.timeslots, function (index, value) {
                        $('.timeslots .table-time').append('<a href="#" class="notranslate time-row' + (value.available === true ? ' time-free' : ' time-ua') + (value.available === true && value.busy === true ? ' time-busy' : '') + '">' + index + '</a>');
                    });
                }
                if (output.overflowTimeslots != undefined) {
                    $.each(output.overflowTimeslots, function (index, value) {
                        $('.timeslots .table-time-overflow').append('<a href="#" class="notranslate time-row' + (value.available === true ? ' time-free' : ' time-ua') + (value.available === true && value.busy === true ? ' time-busy' : '') + '">' + index + '</a>');
                    });
                    $('.timeslots .table-time-overflow, .timeslots .overflow-container').show()
                }
            } else {
            }
            /* hide loading */
            $('.time-selection .loading').hide();
            /* show time-step */
            $('.time-selection, .time-selection .timeslots').fadeIn(effectSpeed);
            if (firstLoad === false) {
                /* scroll to time-step */
                $.scrollTo('.timeslots', 500);
            }
            firstLoad = false;
            /* waanzin */
            if (ticketDate == '2017-05-13' && $('.timeslots-waanzin')) {
                $('.timeslots-waanzin').show();
            }
            /* ajaxRequest */
            ajaxRequest = false;
        }
    });
}

if (typeof (setDate) != 'undefined' && setDate != '') {
    loadTimeslots(setDate);
}

/* show loading */
function showLoading() {
    $('.time-selection, .time-selection .loading').show(effectSpeed);
    setTimeout(function () {
        $('.time-selection .loading').hide();
    }, 2500);
}

/*
* time functions
*/
$('.table-time').on('click', 'a', function () {
    if ($(this).hasClass('time-free')) {
        /* ticketTime */
        var ticketTime = $(this).html();
        $('input#ticketTime').val(ticketTime);
        $('input#overflowTime').val('');
        /* remove all selected class */
        $('.table-time a').removeClass('time-active');
        /* set selected */
        $(this).addClass('time-active');
        /* activate button */
        $('div.ticket-button, div.ticket-button-submit').addClass('active');
    }
    return false;
});
$('.table-time-overflow').on('click', 'a', function () {
    if ($(this).hasClass('time-free')) {
        /* ticketTime */
        var ticketTime = $(this).html();
        $('input#ticketTime').val(ticketTime);
        $('input#overflowTime').val('true');
        /* remove all selected class */
        $('.table-time a').removeClass('time-active');
        /* set selected */
        $(this).addClass('time-active');
        /* activate button */
        $('div.ticket-button, div.ticket-button-submit').addClass('active');
    }
    return false;
});
Date.prototype.yyyymmdd = function () {
    var yyyy = this.getFullYear().toString();
    var mm = (this.getMonth() + 1).toString();
    // getMonth() is zero-based
    var dd = this.getDate().toString();
    return yyyy + '-' + (mm[1] ? mm : "0" + mm[0]) + '-' + (dd[1] ? dd : "0" + dd[0]);
    // padding
}
;

function datepickerMobileDate(next) {
    var date = new Date(Date.parse($('#ticketDate').val()));
    if (next) {
        date.setDate(date.getDate() + 1);
    } else {
        date.setDate(date.getDate() - 1);
    }
    var tomorrow = date.yyyymmdd();
    $('#ticketDate').val(tomorrow);
}

/* mobile datepicker */
$('.date-group .icon-arrow-left').click(function () {
    datepickerMobileDate(false);
    loadTimeslots($('#ticketDate').val());
    return false;
});
$('.date-group .icon-arrow-right').click(function () {
    datepickerMobileDate(true);
    loadTimeslots($('#ticketDate').val());
    return false;
});
/*
* navigation

function setNavTicketDate(date)
{
    if(date == '')
    {
        $('#navDate').html('').hide();
        $('#navDateTime p.hidden-xs').hide();
    }
    else
    {
        $('#navDate').html(date).show();
        $('#navDateTime p.hidden-xs').show();
    }
}

function setNavTicketTime(time)
{
    if(time == '')
    {
        $('#navTime').html('').hide();
    }
    else
    {
        $('#navTime').html(time).show();
    }
}
*/
/*
* button
*/
$('nav.nav-steps').on('click', 'div.ticket-button.active a', function () {
    $('form#gt').submit();
    return false;
});
$('.ticket-zone .container').on('click', 'div.ticket-button-submit.active a', function () {
    $('form#gt').submit();
    return false;
});
$('.donation-zone .container').on('click', 'div.ticket-button-submit.active a', function () {
    $('form#gt').submit();
    return false;
});
/*
* tickets
*/
$('select.tickets-selection').on('change', function () {
    if ($(this).parents('.row-ticketType').find('.descriptionHidden').length > 0) {
        if ($(this).val() > 0) {
            $(this).parents('.row-ticketType').find('.descriptionHidden').show();
        } else {
            $(this).parents('.row-ticketType').find('.descriptionHidden').hide();
        }
    }
    updatePrices();
});
if ($('#tickets_begeleider').length > 0) {
    updatePrices();
}

function updatePrices() {
    var id;
    var amount = 0;
    var ticketsAmount = 0;
    var total, price, ticketsTotal = 0.00;
    var multimediaTours = {};
    var iAmsterdam, countAdult, countYouth = 0;
    var companionTickets = 0;
    var companionSave = $('#tickets_begeleider').val();
    /* nav tickets */
    $('#navTickets').html('');
    $('#navTicketsSummary p.hidden-xs').hide();
    if ($('#tickets_bgl-vip').attr('data-bglmmt') == 'true') {
        $('#row_bgl-vip').attr('data-mmt', 'bglvip');
    }
    if ($('#tickets_begeleider').attr('data-begeleidermmt') == 'true') {
        $('#row_begeleider').attr('data-mmt', 'begeleider');
    }
    /* handle select */
    $('select.tickets-selection').each(function (index, element) {
        /* id */
        id = $(this).attr('id');
        id = id.replace('tickets_', '');
        /* amount */
        amount = $(this).val();
        amount = (amount.length === 0 ? 15 : parseInt(amount));
        if (id == 'iamsterdam' && amount > 0) {
            iAmsterdam = amount;
        }
        if (id == 'adult' && amount > 0) {
            countAdult = amount;
        }
        if (id == 'youth' && amount > 0) {
            countYouth = amount;
        }
        if (isNaN(id) && amount > 0) {
            countAdult = amount;
        }
        /* update ticketsAmount */
        ticketsAmount += amount;
        if (amount > 0) {
            /* calculate total */
            price = parseFloat($('#row_' + id + ' .ticket-price').attr('data-price'));
            total = (amount * price);
            ticketsTotal = ticketsTotal + total;
            /* set total */
            $('#row_' + id + ' span.total').html(number2string(total)).show();
            $('#row_' + id + ' span.euro').show();
            /* description */
            $('#navTickets').append(amount + 'x ' + $('#row_' + id + ' .ticket-description').attr('data-description') + '<br />');
        } else {
            $('#row_' + id + ' span.total').html('&nbsp;').hide();
            $('#row_' + id + ' span.euro').hide();
        }
        if (id != 'begeleider') {
            companionTickets += amount;
        }
        /* multimediaTour */
        var multimediaTour = $('#row_' + id).attr('data-mmt');
        if (multimediaTour != undefined) {
            if (multimediaTour.indexOf(';') != -1) {
                var multimediaTourArray = multimediaTour.split(';');
                $.each(multimediaTourArray, function (key, value) {
                    if (multimediaTours[value]) {
                        multimediaTours[value] = multimediaTours[value] + amount;
                    } else {
                        multimediaTours[value] = amount;
                    }
                    $('#multimediaTour_' + value + ' select').html('');
                });
            } else {
                if (multimediaTours[multimediaTour]) {
                    multimediaTours[multimediaTour] = multimediaTours[multimediaTour] + amount;
                } else {
                    multimediaTours[multimediaTour] = amount;
                }
                $('#multimediaTour_' + multimediaTour + ' select').html('');
            }
        }
    });
    if (companionTickets >= 0) {
        $('#tickets_begeleider').html('');
        for (var i = 0; i <= companionTickets; i++) {
            $('#tickets_begeleider').append('<option value="' + i + '"' + (companionSave == i ? ' selected="selected"' : '') + '>' + i + '</option>');
        }
    }
    if (multimediaTours['bglvip'] == undefined || multimediaTours['bglvip'] == 0) {
        $('#multimediaTour_bglvip').hide();
    } else {
        $('#multimediaTour_bglvip').show();
    }
    if (iAmsterdam > 0) {
        $('a.button .buttonText').hide().next().show();
    } else {
        $('a.button .buttonText').show().next().hide();
    }
    /* set multimediaTour */
    if (ticketsAmount > 0) {
        $.each(multimediaTours, function (id, amount) {
            for (var i = 0; i <= amount; i++) {
                $('#multimediaTour_' + id + ' select').append('<option value="' + i + '"' + (multimediaTourSave[id] && multimediaTourSave[id] == i ? ' selected="selected"' : '') + '>' + i + '</option>');
            }
        });
    }
    if (multimediaTours['begeleider'] == undefined || multimediaTours['begeleider'] == 0) {
        $('#multimediaTour_begeleider').hide();
        $('#multimediaTour_begeleider').find('select').val(0);
    } else {
        $('#multimediaTour_begeleider').show();
        $('#multimediaTour_begeleider').find('select').attr('readonly', 'readonly').val(multimediaTours['begeleider']);
    }
    /* tickets amount */
    $('#add-tickets').attr('data-ticketsamount', ticketsAmount);
    if (ticketsAmount == 0) {
        // $('#tickets-part2').hide(effectSpeed);
        $('#add-tickets').removeClass('active');
        if (!$('#add-tickets').is(':visible')) {
            $('div.ticket-button, div.ticket-button-submit').removeClass('active');
        }
    } else {
        /* nav tickets
        $('#navTickets').show();
        $('#navTicketsSummary p.hidden-xs').show();
        */
        if (!$('#add-tickets').is(':visible') && ticketsAmount > 0) {
            /* activate button */
            $('div.ticket-button, div.ticket-button-submit').addClass('active');
        }
        /* show add-tickets button */
        $('#add-tickets').addClass('active');
    }
    /* update tickets total */
    $('#add-tickets').attr('data-ticketstotal', ticketsTotal);
    /* update total */
    updateTotal();
}

if ($('select.tickets-selection').length > 0) {
    updatePrices();
}
$('#add-tickets').click(function () {
    if ($('#add-tickets').hasClass('active')) {
        /* show tickets part 2 */
        $('#tickets-part2').show(effectSpeed);
        /* activate button */
        $('div.ticket-button, div.ticket-button-submit').addClass('active');
        /* scroll to multimediatour */
        $.scrollTo(document.getElementById('tickets-part2'), 500);
    }
    return false;
});
$('body#tickets a.ticket-info__more-options').click(function () {
    $('.row-ticketType').show(effectSpeed);
    $(this).hide();
    return false;
});
$('body#datetime a.ticket-info__more-options').click(function () {
    $('ul.icons-payment li').show(effectSpeed);
    $(this).hide();
    return false;
});
/*
* multimediatour
*/
$('select.multimediaTour-selection').on('change', function () {
    updateMultimediaTour();
});
$('select#people-select').on('change', function () {
    var ticketsAmount = $(this).val();
    if (ticketsAmount >= 15) {
        $('#dialogGroup').dialog('open');
    }
});

function updateMultimediaTour() {
    var id;
    var amount = 0;
    var multimediaTourAmount = 0;
    var total, price, multimediaTourTotal = 0.00;
    /* reset */
    multimediaTourSave = {};
    /* select handle */
    $('select.multimediaTour-selection').each(function (index, element) {
        /* id */
        id = $(this).attr('id');
        id = id.replace('select_', '');
        /* amount */
        amount = parseInt($(this).val());
        if (amount > 0) {
            multimediaTourAmount += amount;
            /* calculate total */
            price = parseFloat($('#' + id + ' .multimediaTour-price').attr('data-price'));
            total = (amount * price);
            multimediaTourTotal = multimediaTourTotal + total;
            /* set total */
            $('#multimediaTour_' + id + ' span.total').html(number2string(total)).show();
            $('#multimediaTour_' + id + ' span.euro').show();
            /* description */
            $('#navMultimediaTour').append(amount + 'x ' + $('#multimediaTour_' + id + ' .ticket-description').attr('data-description') + '<br />');
        } else {
            $('#multimediaTour_' + id + ' span.total').html('&nbsp;').hide();
            $('#multimediaTour_' + id + ' span.euro').hide();
        }
        /* save multimediaTour */
        multimediaTourSave[id] = amount;
    });
    if (multimediaTourAmount == 0) {
        $('#navMultimediaTour').html('').hide();
        if ($('#add-tickets[name="mmtOnlyActive"]').length > 0) {
            /* activate button */
            $('div.ticket-button, div.ticket-button-submit').removeClass('active');
        }
    } else {
        $('#navMultimediaTour').html(multimediaTourAmount + 'x ' + LANG_MULTIMEDIATOUR).show();
        if ($('#add-tickets[name="mmtOnlyActive"]').length > 0) {
            /* activate button */
            $('div.ticket-button, div.ticket-button-submit').addClass('active');
        }
    }
    /* update multimediatour total */
    $('#add-tickets').attr('data-multimediatourtotal', multimediaTourTotal);
    /* update total */
    updateTotal();
}

/*
* updateTotal
*/
function updateTotal() {
    /* total */
    var total = parseFloat($('#add-tickets').attr('data-ticketstotal'));
    /* donation */
    var donation = '';
    donation = donation.replace('-', '');
    donation = donation.replace(',', '.');
    donation = parseFloat(donation);
    if (donation > 0) {
        total += donation;
    }
    /* set total */
    $('#total span.total').html(number2string(total)).show();
    $('#total span.euro').show();
    if (total > 0) {
        $('div.ticket-button, div.ticket-button-submit').addClass('active');
    }
}

$('#clearVoucherButton').on('click', function () {
    $('#voucherForm').submit();
    return false;
});
/*
* donation
*/
$('input#donation').keypress(function () {
    setTimeout(function () {
        updateTotal()
    }, 50);
});
$('.ticket-description > a').on('click', function () {
    var hiddenDesc = $(this).parents('.row-ticketType').find('.descriptionHidden');
    if ($(hiddenDesc).is(':visible')) {
        $(hiddenDesc).hide();
    } else {
        $(hiddenDesc).show();
    }
    return false;
});
/*
* MMT video
*/
$('#open-video').click(function () {
    $('#dialogMMT').dialog('open');
    return false;
});
/*
* dialogs
*/
$('#dialogGroup').dialog({
    autoOpen: false,
    modal: true,
    width: 320,
    height: 180,
    open: function (event, ui) {
        $('.ui-dialog-titlebar').css('display', 'none');
    }
});
$('#dialogMMT').dialog({
    autoOpen: false,
    modal: true,
    width: 850,
    height: 475,
    open: function (event, ui) {
        $('.ui-dialog-titlebar').css('display', 'none');
        setTimeout(playVideo, 1000);
    }
});
$('#dialogDateMin').dialog({
    autoOpen: false,
    modal: true,
    width: 320,
    height: 180,
    open: function (event, ui) {
        $('.ui-dialog-titlebar').css('display', 'none');
    }
});
$('#dialogDateMax').dialog({
    autoOpen: false,
    modal: true,
    width: 320,
    height: 180,
    open: function (event, ui) {
        $('.ui-dialog-titlebar').css('display', 'none');
    }
});
$('#iamsterdam-card-dialog').dialog({
    autoOpen: false,
    modal: true,
    width: 400,
    height: 240,
    open: function (event, ui) {
        $('.ui-dialog-titlebar').css('display', 'none');
    }
});
$('#museumkaart-card-dialog').dialog({
    autoOpen: false,
    modal: true,
    width: 480,
    height: 300,
    open: function (event, ui) {
        $('.ui-dialog-titlebar').css('display', 'none');
    }
});
$('#iamsterdam-info').on('click', function (e) {
    $('#iamsterdam-card-dialog').dialog('open');
    e.preventDefault();
});
$('#museumkaart-info').on('click', function (e) {
    $('#museumkaart-card-dialog').dialog('open');
    e.preventDefault();
});
$('.close-button').click(function (e) {
    e.preventDefault();
    $('#dialogGroup').dialog('close');
    $('#dialogMMT').dialog('close');
    $('#dialogDateMin').dialog('close');
    $('#dialogDateMax').dialog('close');
    $('#iamsterdam-card-dialog').dialog('close');
    $('#museumkaart-card-dialog').dialog('close');
    //stopVideo();
    return false;
});
$('body').on('click', '.ui-widget-overlay', function () {
    $('#dialogGroup').dialog('close');
    $('#dialogMMT').dialog('close');
    $('#dialogDateMax').dialog('close');
    $('#dialogDateMin').dialog('close');
    $('#iamsterdam-card-dialog').dialog('close');
    $('#museumkaart-card-dialog').dialog('close');
    //stopVideo();
    return false;
});

/*
* yourinformation
*/
function termsandconditionsCheck() {
    if (!$('#termsandconditions').length) {
        return;
    }
    var checked = $('#termsandconditions').is(':checked')
        , buttons = $('div.ticket-button, div.ticket-button-submit');
    checked ? buttons.addClass('active') : buttons.removeClass('active');
}

termsandconditionsCheck();
$('#termsandconditions').on('change', function (e) {
    termsandconditionsCheck();
});
/*
* check
*/
$('.paymentmethods[id!="component-container"] input[name="paymentradio"]').change(function () {
    /* activate button */
    $('div.ticket-button, div.ticket-button-submit').addClass('active');
    /* submit */
    $('form#gt').submit();
    return false;
});
$('body#check a.ticket-info__more-options').click(function () {
    $('div.paymentmethods div').show(effectSpeed);
    $(this).hide();
    return false;
});
$('#cookie-container .buttons-wrap a').on('click', function (e) {
    e.preventDefault();
    var cookieVal = 0;
    if ($(this).attr('id') == 'cookiesAccept') {
        cookieVal = 1;
    }
    var d = new Date();
    d.setTime(d.getTime() + (10 * 365 * 24 * 60 * 60 * 1000));
    var cookieDomain = (window.location.hostname ? window.location.hostname : '.vangoghmuseum.com');
    cookieDomain = cookieDomain.replace('tickets', '');
    document.cookie = '.GdprCookie.V1' + '=' + cookieVal + ';expires=' + d.toUTCString() + ';domain=' + cookieDomain + ';path=/;secure';
    $('#cookie-container').hide('fast');
});

/* changes */
function videoScroll() {
    var videoPos = $('a.open-video').offset()
        , videoHeight = $('a.open-video').outerHeight() / 2;
    $(window).on('scroll', function () {
        if ($('#tickets-part2').is(':visible')) {
            var windowHeight = $(window).height() / 2
                , windowTop = $(window).scrollTop()
                , windowPos = windowHeight + windowTop;
            if (videoPos.top + videoHeight > windowPos) {
                $('a.open-video').css({
                    'position': 'static'
                });
            }
            if (windowPos > videoPos.top + videoHeight) {
                $('a.open-video').css({
                    'position': 'fixed',
                    'top': windowHeight - videoHeight,
                    'left': videoPos.left
                });
            }
            if ($('a.open-video').offset().top > $('#tickets-part2').offset().top) {
                var offset = $('a.open-video').offset().top;
                $('a.open-video').css({
                    'position': 'absolute',
                    'top': $('.ticket-blue').offset().top,
                    'left': videoPos.left
                });
            }
        } else {
            $('a.open-video').css({
                'position': 'static',
                'top': 'auto',
                'left': 'auto'
            });
        }
    });
}

function stepsHover() {
    $('.nav-steps .steps').hover(function () {
        if ($('#navDateTime').has('p.sub.hidden-xs').length) {
            $('.sub.hidden-xs').stop(true).slideDown('fast');
        }
    }, function () {
        if ($('#navDateTime').has('p.sub.hidden-xs').length) {
            $('.sub.hidden-xs').stop(true).slideUp('fast');
        }
    });
}

function steps() {
    displayMobileFooter();
    $(window).on('scroll', function () {
        displayMobileFooter();
    });
}

function displayMobileFooter() {
    if ($('.ticket-button').hasClass('mobile')) {
        var scrollTop = $(window).scrollTop()
            , headerPos = $('header').offset().top + $('header').height();
        if (scrollTop > headerPos || $(document).height() - headerPos <= $(window).height()) {
            $('.ticket-button.mobile').slideDown('fast');
        }
    }
}

function mobile_leesmeer_button() {
    $('#mobile_leesmeer_button').on('click', function (e) {
        e.preventDefault();
        var leesmeer = $('#mobile_leesmeer_button').offset().top;
        $('#mobile_leesmeer').show();
        if (!$(this).hasClass('active')) {
            $(this).hide();
            $(window).animate({
                scrollTop: (leesmeer - 50)
            }, 'slow');
        } else {
            $(this).addClass('active');
        }
    });
}

function copypaste() {
    $(document).on('paste', "#emailAddressConfirm", function (e) {
        e.preventDefault();
    });
}

copypaste();

function ticketDate() {
    var $ticketDate = $('#ticketDate')
        , maxDate = $ticketDate.attr('max')
        , minDate = $ticketDate.attr('min')
        , maanden = ["januari", "februari", "maart", "april", "mei", "juni", "july", "augustus", "september", "oktober", "november", "december"]
        , months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"]
        , a = new Date(minDate)
        , b = new Date(maxDate)
        , minDate_text = a.getDate() + ' ' + maanden[a.getMonth()] + ' ' + a.getFullYear()
        , maxDate_text = b.getDate() + ' ' + maanden[b.getMonth()] + ' ' + b.getFullYear()
        , message = 'Tickets zijn alleen verkrijgbaar van ' + minDate_text + ' tot en met ' + maxDate_text;

    function ticketDate_message() {
        // Als het gekozen datum NIET binnen het aangegeven datum valt, dan komt er een waarschuwing.
        // Als het gekozen datum WEL binnen het aangegeven datum valt, dan kom de timetable.
        if ($($ticketDate).val() < minDate || $($ticketDate).val() > maxDate) {
            if ($($ticketDate).val() < minDate) {
                $($ticketDate).val(minDate);
                $('#dialogDateMin').dialog('open');
            } else if ($($ticketDate).val() > maxDate) {
                $($ticketDate).val(maxDate);
                $('#dialogDateMax').dialog('open');
            }
        } else {
            loadTimeslots($('#ticketDate').val());
        }
    }

    var ticketDate_msg = {
        ios: function () {
            $ticketDate.on('blur', function () {
                //Focus veranderd
                ticketDate_message();
            });
        },
        android: function () {
            $ticketDate.on('change', function () {
                //Value veranderd
                ticketDate_message();
            });
        }
    };
    var userAgent = navigator.userAgent || navigator.vendor || window.opera;
    if (userAgent.match(/iPad/i) || userAgent.match(/iPhone/i) || userAgent.match(/iPod/i)) {
        ticketDate_msg.ios();
    } else if (userAgent.match(/Android/i)) {
        ticketDate_msg.android();
    } else {
        ticketDate_msg.android();
    }
}

function browser() {
    if (768 > $(window).width()) {
        steps();
        mobile_leesmeer_button();
        ticketDate();
        /*
        if($('#ticketDate').length > 0 && $('.table-time').find('a').length == 0)
        {
            firstLoad = true;
            loadTimeslots($('#ticketDate').val());
        }
*/
    } else {
        $('.ticket-button').removeClass('mobile').slideDown('fast');
        //function steps()
        stepsHover();
        // videoScroll();
    }
}

$(window).on('resize', browser);
$(document).ready(browser);
/***************************************
 Dialogs
 ***************************************/
$('.dialog-container').dialog({
    autoOpen: false,
    modal: true,
    resizable: false,
    draggable: false,
    width: '100%',
    open: function () {
        $('body').css('overflow', 'hidden');
    },
    close: function () {
        $('body').css('overflow', 'visible');
    }
});
$('[data-dialog]').on('click', function (e) {
    var dialog = $(this).data('dialog');
    $(dialog).dialog('open');
    e.preventDefault();
});
$('.close-dialog').on('click', function (e) {
    $('.dialog-container').dialog('close');
    e.preventDefault();
});
/***************************************
 Donation
 ***************************************/
var donationColumnLeft = $('.donatie .col-xs-8').addClass('donationOther');
var donationColumnRight = $('.donatie .col-xs-4').addClass('donationOther');
$(donationColumnRight).removeClass('col-xs-4').addClass('col-xs-12 text--right');
$(donationColumnLeft).remove();
$('.vgm-donation').click(function (e) {
    $('.vgm-donation').removeClass('vgmActive');
    var donationValue = $(this).data('donationvalue');
    $(this).addClass('vgmActive');
    if (donationValue == 'other') {
        $('input#donation').val('0.00').trigger(jQuery.Event('keypress', {
            keycode: 13
        }));
        $('.donatie .donationOther').show();
    } else {
        $('input#donation').val(donationValue).trigger(jQuery.Event('keypress', {
            keycode: 13
        }));
        $('.donatie .donationOther').hide();
    }
    e.preventDefault();
});
// select button on pageLoad
var currentDonation = $('input#donation').val();
if (currentDonation != '0.00' && currentDonation != '0,00') {
    var currentDonationButton = $('.vgm-donation[data-donationvalue="' + currentDonation + '"]');
    var currentDonationButtonLenght = $(currentDonationButton).length;
    if (currentDonationButtonLenght >= 1) {
        $(currentDonationButton).addClass('vgmActive');
    } else {
        $('.vgm-donation[data-donationvalue="other"]').addClass('vgmActive');
        $('.donatie .donationOther').show();
    }
}

