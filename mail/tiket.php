<html lang="id">
<head>
    <title>Tiket Kunjungan Ullen Sentalu</title>
</head>
<body>
<div dir="ltr">Halo <?=$completeName?>,
    <br>
    <br>
    <div>
        <p>
            Pembayaran Via <?=$bank?> sudah kami terima. <br>
            Dengan ini kami kirim tiket sebagai berikut :<br>
        <pre>
============================================================

<strong>E-TIKET MUSEUM ULLEN SENTALU
NOMOR : <?=strtoupper($tiket);?></strong>
Grup atas nama	: <?=$completeName."\n"?>
No Reservasi	: <?=$no_booking."\n"?>
No Invoice	: <?=$no_invoice."\n"?>
Jenis Tour	: <?=$tourSelect."\n"?>
Tanggal		: <?=$ticketDate."\n"?>
Waktu		: <?=$ticketTime."\n"?>
<? if($qtyDewasa != 0) echo "Dewasa         : ".$qtyDewasa."\n"?>
<? if($qtyAnak != 0) echo "Anak         : ".$qtyAnak."\n"?>

============================================================
<strong>HAL PENTING YANG PERLU DIPERHATIKAN :</strong>
Dimohon anda sudah tiba di Museum 15 menit sebelumnya.
Jika cuaca mendung atau musim hujan tiba, jangan lupa membawa payung.

Selamat berkunjung di Museum Ullen Sentalu.
============================================================
        </pre>
        </p>
    </div>
    <div>
        <div><br></div>
        Salam,
    </div>
    <div><br></div>
    <div>Admin Museum Ullen Sentalu<br></div>
    <div><br>
    </div>
    <div><br class="gmail-Apple-interchange-newline"></div>
</div>
</body>
</html>