<html lang="id">
<head>
    <title>Menunggu Pembayaran Reservasi Ullen Sentalu</title>
</head>
<body>
<div dir="ltr">Halo <?=$completeName?>,
    <div>Terima kasih ...</div>
    <div>Reservasi anda sudah kami proses sebagai berikut :
        <table style="width: 50%;">
            <tbody>
            <tr>
                <td style="width: 25%;">No Reservasi
                    <br>
                </td>
                <td style="width: 75%;">: <?=$no_booking;?>
                    <br>
                </td>
            </tr>
            <tr>
                <td style="width: 25%;">Jenis Tour
                    <br>
                </td>
                <td style="width: 75%;">: <?=$tourSelect;?>
                    <br>
                </td>
            </tr>
            <tr>
                <td style="width: 25%;">Tanggal Reservasi
                    <br>
                </td>
                <td style="width: 75%;">: <?=$ticketDate;?>
                    <br>
                </td>
            </tr>
            <tr>
                <td style="width: 25%;">Waktu Tour
                    <br>
                </td>
                <td style="width: 75%;">: <?=$ticketTime;?></td>
            </tr>
            </tbody>
        </table>
        <table style="width: 50%;">
            <tbody>
            <tr>
                <td style="width: 30%;">Dewasa
                    <br>
                </td>
                <td style="width: 25%;">Rp <?=number_format($hargaDewasa,0,',','.');?>
                    <br>
                </td>
                <td style="width: 10%;text-align: center;"><?=$qtyDewasa;?>
                    <br>
                </td>
                <td style="width: 25%;text-align: right;">Rp <?=number_format($totDewasa,0,',','.');?>
                    <br>
                </td>
            </tr>
		    <? if($qtyAnak > 0) : ?>
                <tr>
                    <td>Anak (5-12 th)
                        <br>
                    </td>
                    <td>Rp<?=number_format($hargaAnak,0,',','.');?>
                        <br>
                    </td>
                    <td style="text-align: center;"><?=$qtyAnak;?>
                        <br>
                    </td>
                    <td style="text-align: right;">Rp <?=number_format($totAnak,0,',','.');?>
                        <br>
                    </td>
                </tr>
		    <? endif; ?>
            <tr>
                <td colspan="3" style="font-weight: bold;">Nilai Bayar
                    <br>
                </td>
                <td style="font-weight: bold;text-align: right;">Rp <?=number_format($total,0,',','.');?>
                    <br>
                </td>
            </tr>
            </tbody>
        </table>
    </div>
    <div><br></div>
    <div>Silahkan melakukan pembayaran <b>maksimal 1 jam</b>
    </div>
    <div>setelah reservasi <b>(<?=$expired;?> WIB)</b> Melalui :
        <table style="width: 50%;font-weight: bold;">
            <tbody>
            <tr>
                <td style="width: 33%;padding:10px;">
                    Bank Mandiri<br>
                    KCP Yogyakarta<br>
                    Diponegoro<br>
                    137-00-8585000-2<br>
                    an CV Arum Djagad<br>
                    <img src="https://tickets.ullensentalu.com/img/mandiri-code.png" alt="" width="100px" height="100px"/>
                </td>
<!--                <td style="width: 34%;padding:10px;-->
<!--                                        border-right-style: solid;border-right-width: thin;border-right-color: grey;-->
<!--                                        border-left-style: solid;border-left-width: thin;border-left-color: grey;">-->
<!--                    Bank Mandiri<br>-->
<!--                    KCP Yogyakarta<br>-->
<!--                    Diponegoro<br>-->
<!--                    137-00-8585000-2<br>-->
<!--                    an CV Arum Djagad<br>-->
<!--                    <img src="https://tickets.ullensentalu.com/img/mandiri-code.png" alt="" width="100px" height="100px"/>-->
<!--                </td>-->
<!--                <td style="width: 33%;padding:10px;">-->
<!--                    Bank BCA<br>-->
<!--                    888-777-6666<br>-->
<!--                    an CV Arum Djagad<br>-->
<!--                    <img src="https://tickets.ullensentalu.com/img/bca-code.png" alt="" width="100px" height="100px"/>-->
<!--                </td>-->
            </tr>
            </tbody>
        </table>
    </div>
    <div><br></div>
    <div>Setelah melakukan pembayaran, kirim bukti transfer disertai kode reservasi ke:
    </div>
    <div>
        Whatsapp <a href="https://wa.me/62<?=str_replace(' ','',Yii::$app->params['waConfirm'])?>">+62 <?=Yii::$app->params['waConfirm']?></a> atau email <a href="mailto:<?=Yii::$app->params['emailConfirm']?>"><?=Yii::$app->params['emailConfirm']?></a> <br>
        dengan mengirimkan: <br>
        1. Foto bukti transfer <br>
        2. Disertai pesan “No Reservasi (spasi) tgl kunjung (spasi) Bank (spasi) Nama Rekening Pengirim” <br>
        Contoh: “<?=$no_booking;?> <?=$ticketDate;?> MANDIRI <?=$completeName?>” <br>
        3. Konfirmasi transfer dilakukan maksimal 1 jam setelah transfer <br>
        4. e-Tiket akan dikirimkan ke email setelah transfer dikonfirmasi <br>
        5. Jika anda bersedia menerima informasi lebih lanjut dari kami (join mailing list)<br>
        mohon tambahkan pesan “ IKUT ML “ pada saat mengirim foto bukti transfer. <br>
        Contoh: “<?=$no_booking;?> <?=$ticketDate;?> MANDIRI <?=$completeName?>” IKUT ML<br>
        <br>
        Untuk info lebih lanjut hubungi WA 0<?=Yii::$app->params['waConfirm']?> atau 0274 880158
    <div>
        <div><br></div>
        Salam,
    </div>
    <div><br></div>
    <div>Admin Museum Ullen Sentalu<br></div>
    <div><br>
    </div>
    <div><br class="gmail-Apple-interchange-newline"></div>
</div>
</body>
</html>