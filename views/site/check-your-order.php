<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Book your tickets - Ullen Sentalu Museum';
$session = Yii::$app->session;
$session->open();
$isPage1Done = $session->has('page_1');
$isPage2Done = $session->has('page_2');
$isPage3Done = $session->has('page_3');
$isPage4Done = $session->has('page_4');
?>
<script>
    var language = 'en';
    var jsDomain = '';
    var ticketDateMin = new Date(2020, 6, 20);
    var ticketDateMax = new Date(2020, 8, 30);
    var disabledDays = [];
    var disabledDates = [];
    var soldoutDays = ["2020-07-20"];
</script>

<form id="gt" method="post" action="https://tickets.vangoghmuseum.com/en/payment">
	<?= $this->render( '_nav-bottom', [
		'step' => 4,
		'buttonText' => ''
	] ) ?>
    <section class="ticket-zone">
        <div class="container">
            <div class="row">
                <div class="col-sm-12 col-md-7">
                    <h2>Please check your order</h2>

                    <div class="row check-info">
                        <div class="col-sm-6">
                            <p><strong>Date &amp; time</strong>
                                <a href="<?=url::toRoute(['site/index'])?>" class="change-link">Change</a>
                            </p>
                            <p><?=($isPage1Done ? date('d F', strtotime($session->get('page_1')['ticketDate'])) : '') ?><br>
	                            <?=($isPage1Done ? $session->get('page_1')['ticketTime'] : '') ?></p>
                        </div>
                        <div class="col-sm-6">
                            <p><strong>Your information</strong> <a href="<?=url::toRoute(['site/your-information'])?>" class="change-link">Change</a></p>
                            <p><?=($isPage3Done) ? $session->get('page_3')['firstName'].' '.$session->get('page_3')['surName']:''?><br>
	                            <?=($isPage3Done) ? $session->get('page_3')['emailAddressConfirm'] : '' ?></p>
                        </div>
                    </div> <!-- row -->

                    <div class="tickets check-zone">
                        <div class="row">
                            <div class="ticket__header cf">
                                <div class="col-md-12">
                                    <p><strong>Tickets</strong> <a href="<?=url::toRoute(['site/tiket'])?>" class="change-link">Change</a></p>
                                </div>
                            </div> <!-- ticket__header -->
                        </div> <!-- row -->
                        <div class="row">
                            <div class="ticket__cat cf">
	                            <?if($session->get('page_2')['tickets']['ra'] > 0):?>
                                <div class="col-xs-7 col-sm-8">
                                    <span class="pull-left">Adiluhung Mataram Adult</span>
                                    <span class="pull-right">x <?=$session->get('page_2')['tickets']['ra']?></span>
                                </div>
                                <div class="col-xs-5 col-sm-4">
                                    <p class="text--right">Rp<?=number_format(Yii::$app->params['ra']*$session->get('page_2')['tickets']['ra'],0,',','.')?></p>
                                </div>
	                            <?endif;?>
	                            <?if($session->get('page_2')['tickets']['rc'] > 0):?>
                                <div class="col-xs-7 col-sm-8">
                                    <span class="pull-left">Adiluhung Mataram Child</span>
                                    <span class="pull-right">x <?=$session->get('page_2')['tickets']['rc']?></span>
                                </div>
                                <div class="col-xs-5 col-sm-4">
                                    <p class="text--right">Rp<?=number_format(Yii::$app->params['rc']*$session->get('page_2')['tickets']['rc'],0,',','.')?></p>
                                </div>
	                            <?endif;?>
	                            <?if($session->get('page_2')['tickets']['va'] > 0):?>
                                <div class="col-xs-7 col-sm-8">
                                    <span class="pull-left">Vorstenlanden Adult</span>
                                    <span class="pull-right">x <?=$session->get('page_2')['tickets']['va']?></span>
                                </div>
                                <div class="col-xs-5 col-sm-4">
                                    <p class="text--right">Rp<?=number_format(Yii::$app->params['va']*$session->get('page_2')['tickets']['va'],0,',','.')?></p>
                                </div>
	                            <?endif;?>
	                            <?if($session->get('page_2')['tickets']['vc'] > 0):?>
                                <div class="col-xs-7 col-sm-8">
                                    <span class="pull-left">Vorstenlanden Child</span>
                                    <span class="pull-right">x <?=$session->get('page_2')['tickets']['vc']?></span>
                                </div>
                                <div class="col-xs-5 col-sm-4">
                                    <p class="text--right">Rp<?=number_format(Yii::$app->params['vc']*$session->get('page_2')['tickets']['vc'],0,',','.')?></p>
                                </div>
	                            <?endif;?>
                            </div> <!-- ticket__cat -->
                        </div>
                        <div class="row">
                            <div class="ticket__cat cf ticket__borderless">
                            </div>
                        </div>

                        <div class="row">
                            <div class="ticket__cat cf ticket__borderless">
                            </div>
                        </div>

                        <div class="row">
                            <div class="ticket__subtotal cf ticket__borderless">
                                <div class="col-xs-7 col-sm-8">
                                    <p><strong>Total</strong></p>
                                </div>
                                <div class="col-xs-5 col-sm-4">
                                    <p class="text--right"><strong>Rp<?=number_format($session->get('page_2')['total'],0,',','.')?></strong></p>
                                </div>
                            </div> <!-- ticket__subtotal -->
                        </div> <!-- row -->

                    </div> <!-- tickets -->
                    <button class="adyen-checkout__button adyen-checkout__button--pay" type="button">
                        <span class="adyen-checkout__button__content">
                            <img class="adyen-checkout__button__icon" src="https://checkoutshopper-live.adyen.com/checkoutshopper/images/components/lock.svg" alt="Icon" aria-hidden="true" role="presentation">
                            <span class="adyen-checkout__button__text">Pay Rp<?=number_format($session->get('page_2')['total'],0,',','.')?></span>
                        </span>
                    </button>
                    <br style="clear:both;">
                </div>

                <div class="col-sm-12 col-md-5">

                </div>
            </div>

        </div>
    </section>			<footer class="footer">

    </footer>
</form>
<?php $session->close();