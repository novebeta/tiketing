<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Book your tickets - Ullen Sentalu Museum';
?>
<script>
    var language = 'en';
    var jsDomain = '';
    var ticketDateMin = new Date(2020, 6, 20);
    var ticketDateMax = new Date(2020, 8, 30);
    var disabledDays = [];
    var disabledDates = [];
    var soldoutDays = [""];
</script>
<form id="gt" method="post" action="<?=url::toRoute(['site/check-your-order'])?>" _lpchecked="1">
<!--	--><?//= $this->render( '_nav-bottom', [
//		'step' => 3,
//		'buttonText' => '<span class="visible-xs-inline">4. </span> <span class="buttonText">Payment</span>'
//	] ) ?>
    <section class="ticket-zone">
        <div class="container">
	        <?php if (Yii::$app->session->hasFlash('error')): ?>
                <div class="alert alert-danger alert-dismissable">
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                    <h4><i class="icon fa fa-check"></i>Ooopppss something wrong</h4>
			        <?= Yii::$app->session->getFlash('error') ?>
                </div>
	        <?php endif; ?>
            <div class="row">
                <div class="col-sm-8">
                    <h1 class="subtitle">MENUNGGU PEMBAYARAN</h1>

                    <h2 class="subtitle">No Reservasi: <?=$no_booking;?></h2>

                    <div class="tickets">
                        <div class="form-persons">
                            <div class="row">
                                <div class="ticket__header cf">
                                    <div class="col-md-12">
                                        <h3>Tur <?=$tourSelect;?> </h3>
                                    </div>
                                </div>
                            </div>
                            <table style="width: 100%;">
                                <tbody>
                                <tr>
                                    <td style="width: 25%;">Tanggal
                                        <br>
                                    </td>
                                    <td style="width: 75%;">: <?=$ticketDate;?>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">Jam Tur
                                        <br>
                                    </td>
                                    <td style="width: 75%;">: <?=$ticketTime;?></td>
                                </tr>
                                </tbody>
                            </table>
                            <div style="margin-bottom: 25px"></div>
                            <div class="row">
                                <div class="ticket__header cf">
                                    <div class="col-md-12">
                                        <h3>Kategori Tiket</h3>
                                    </div>
                                </div>
                            </div>
                            <table style="width: 75%;">
                                <tbody>
                                <tr>
                                    <td style="width: 30%;">Dewasa
                                        <br>
                                    </td>
                                    <td style="width: 25%;">Rp <?=number_format($hargaDewasa,0,',','.');?>
                                        <br>
                                    </td>
                                    <td style="width: 10%;text-align: center;"><?=$qtyDewasa;?>
                                        <br>
                                    </td>
                                    <td style="width: 25%;text-align: right;">Rp <?=number_format($totDewasa,0,',','.');?>
                                        <br>
                                    </td>
                                </tr>
                                <? if($qtyAnak > 0) : ?>
                                <tr>
                                    <td>Anak (5-12 th)
                                        <br>
                                    </td>
                                    <td>Rp<?=number_format($hargaAnak,0,',','.');?>
                                        <br>
                                    </td>
                                    <td style="text-align: center;"><?=$qtyAnak;?>
                                        <br>
                                    </td>
                                    <td style="text-align: right;">Rp <?=number_format($totAnak,0,',','.');?>
                                        <br>
                                    </td>
                                </tr>
                                <? endif; ?>
                                <tr>
                                    <td colspan="3">NILAI BAYAR
                                        <br>
                                    </td>
                                    <td style="text-align: right;">Rp <?=number_format($total,0,',','.');?>
                                        <br>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div style="margin-bottom: 25px"></div>
                            <div class="row">
                                <div class="ticket__header cf">
                                    <div class="col-md-12">
                                        <h3>Data Identitas</h3>
                                    </div>
                                </div>
                            </div>
                            <table style="width: 100%;">
                                <tbody>
                                <tr>
                                    <td style="width: 25%;">Nama Lengkap
                                        <br>
                                    </td>
                                    <td style="width: 75%;">: <?=$completeName;?>
                                        <br>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">No. HP (WA)
                                        <br>
                                    </td>
                                    <td style="width: 75%;">: <?=$handphone;?></td>
                                </tr>
                                <tr>
                                    <td style="width: 25%;">Email
                                        <br>
                                    </td>
                                    <td style="width: 75%;">: <?=$emailAddress;?>
                                        <br>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                            <div style="margin-bottom: 25px"></div>
                            <div class="row">
                                <div class="ticket__header cf">
                                    <div class="col-md-12">
                                        <h3><strong>CEK EMAIL</strong></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-left: 15px;">
                                <strong>RESERVASI ANDA TELAH KAMI PROSES,</strong> <i>selanjutnya silahkan <strong>CEK EMAIL</strong> anda untuk prosedur pembayaran.</i> Terima Kasih, selamat berkunjung...
                            </div>
                            <div style="margin-bottom: 25px"></div>
                            <div class="row">
                                <div class="ticket__header cf">
                                    <div class="col-md-12">
                                        <h3><strong>Pembayaran</strong></h3>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-left: 15px;">
                                Pembayaran tiket dilakukan melalui transfer bank: <br>
                                <span style="font-weight: bold;padding-left: 10px;">Nilai Bayar&emsp;&emsp;: Rp <?=number_format($total,0,',','.');?></span> <br>
                                <span style="font-weight: bold;padding-left: 10px;">Batas Waktu&emsp;: <?=$expired;?> WIB</span> <br>
                                Reservasi tiket akan BATAL bila transfer tidak dilakukan sebelum  <br>
                                batas waktu pembayaran.  <br>
                                <table style="width: 100%;font-weight: bold;">
                                    <tbody>
                                    <tr>
                                        <td style="width: 33%;padding:10px;">
                                            Bank Mandiri<br>
                                            KCP Yogyakarta<br>
                                            Diponegoro<br>
                                            137-00-8585000-2<br>
                                            an CV Arum Djagad<br>
                                            <img src="https://tickets.ullensentalu.com/img/mandiri-code.png" alt="" width="100px" height="100px"/>
                                        </td>
<!--                                        <td style="width: 34%;padding:10px;-->
<!--                                        border-right-style: solid;border-right-width: thin;border-right-color: grey;-->
<!--                                        border-left-style: solid;border-left-width: thin;border-left-color: grey;">-->
<!--                                            Bank Mandiri<br>-->
<!--                                            KCP Yogyakarta<br>-->
<!--                                            Diponegoro<br>-->
<!--                                            137-00-8585000-2<br>-->
<!--                                            an CV Arum Djagad<br>-->
<!--                                            <img src="https://tickets.ullensentalu.com/img/mandiri-code.png" alt="" width="150px" height="150px"/>-->
<!--                                        </td>-->
<!--                                        <td style="width: 33%;padding:10px;">-->
<!--                                            Bank BCA<br>-->
<!--                                            888-777-6666<br>-->
<!--                                            an CV Arum Djagad<br>-->
<!--                                            <img src="https://tickets.ullensentalu.com/img/bca-code.png" alt="" width="150px" height="150px"/>-->
<!--                                        </td>-->
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div style="margin-bottom: 25px"></div>
                            <div class="row">
                                <div class="ticket__header cf">
                                    <div class="col-md-12">
                                        <h3>Konfirmasi</h3>
                                    </div>
                                </div>
                            </div>
                            <div class="row" style="padding-left: 15px;">
                                Konfirmasi transfer pembayaran Anda melalui <br>
                                Whatsapp <a href="https://wa.me/62<?=str_replace(' ','',Yii::$app->params['waConfirm'])?>">+62 <?=Yii::$app->params['waConfirm']?></a> atau email <a href="mailto:<?=Yii::$app->params['emailConfirm']?>"><?=Yii::$app->params['emailConfirm']?></a> <br>
                                dengan mengirimkan: <br>
                                1. Foto bukti transfer <br>
                                2. Disertai pesan “No Reservasi (spasi) tgl kunjung (spasi) Bank (spasi) Nama Rekening Pengirim” <br>
                                Contoh: “<?=$no_booking;?> <?=$ticketDate;?> MANDIRI <?=$completeName?>” <br>
                                3. Konfirmasi transfer dilakukan maksimal 1 jam setelah transfer <br>
                                4. e-Tiket akan dikirimkan ke email setelah transfer dikonfirmasi <br>
                                5. Jika anda bersedia menerima informasi lebih lanjut dari kami (join mailing list)<br>
                                mohon tambahkan pesan “ IKUT ML “ pada saat mengirim foto bukti transfer. <br>
                                Contoh: “<?=$no_booking;?> <?=$ticketDate;?> MANDIRI <?=$completeName?>” IKUT ML<br>
                                <br>
                                Untuk info lebih lanjut hubungi WA 0<?=Yii::$app->params['waConfirm']?> atau 0274 880158
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-sm-12 col-md-5">

                </div>
            </div>

        </div>
    </section>
    <footer class="footer">

    </footer>
</form>
<?php
$this->registerJs( <<< JS

    $( "#gt" ).submit(function( event ) {
     // alert( "Handler for .submit() called." );
     // event.preventDefault();
    });

JS
);
