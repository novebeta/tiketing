<?php
/* @var $this yii\web\View */
use yii\bootstrap\Html;
use yii\helpers\Url;
$this->title = 'Book your tickets - Ullen Sentalu Museum';
$this->registerJsVar( '__ticketDateMin', $ticketDateMin );
$this->registerJsVar( '__ticketDateMax', $ticketDateMax );
$this->registerJsVar( 'soldoutDays', $soldoutDays );
$this->registerJsVar( 'disabledDays', $disabledDays );
$this->registerJsVar( 'disabledDates', $disabledDates );
$this->registerJsVar( 'openDays', $openDays );
?>
<script>
    var language = 'in';
    var jsDomain = '';
    var ticketDateMin = new Date(__ticketDateMin);
    var ticketDateMax = new Date(__ticketDateMax);
</script>
<form id="gt" method="post" action="<?= url::toRoute( [ 'site/tiket' ] ) ?>">
    <input id="form-token" type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
           value="<?= Yii::$app->request->csrfToken ?>"/>
    <!--	--><? //= $this->render( '_nav-bottom', [
	//	        'step' => 1,
	//		'buttonText' => '<span class="visible-xs-inline">2. </span> <span class="buttonText">Tickets</span>'
	//	] ) ?>
    <section class="ticket-zone">
        <div class="container">
            <div class="row">
				<?php if ( Yii::$app->session->hasFlash( 'error' ) ): ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4><i class="icon fa fa-check"></i>Ooopppss something wrong</h4>
						<?= Yii::$app->session->getFlash( 'error' ) ?>
                    </div>
				<?php endif; ?>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-7-datepicker">
                    <h1>BELI TIKET TOUR MUSEUM</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-7-datepicker">
                    <p>Sehubungan dengan penerapan protokol kesehatan COVID-19 maka bagi pengunjung
                        Museum Ullen Sentalu diharap mengikuti prosedur sbb:</p>
                    <ol id="prosedur">
                        <li value="online"><a href="#gt">Pemesanan tiket via online</a></li>
                        <li value="langsung"><a href="#gt">Pemesanan tiket langsung</a></li>
                        <li value="kunjungan"><a href="#gt">Kunjungan</a></li>
                    </ol>
                    <div id="online" class="prosedurDesc" style="display: none;">
                        <p>
                            PROSEDUR PEMESANAN TIKET VIA ONLINE
                            <ol>
                                <li>Pilih jenis tour</li>
                                <li>Pilih tanggal dan waktu tour</li>
                                <li>Masukan jumlah reservasi tiket
                        <p>(maksimal reservasi / kunjungan dalam 1 waktu tour adalah 6 orang – dengan syarat 1 alamat / keluarga)</p>
                        </li>
                        <li>Masukan data diri</li>
                        <li>Pembayaran via :</li>
                        <ul>
                            <li>QRIS</li>
                            <li>Transfer</li>
                        </ul>
                        <li>Kirim bukti transfer disertai kode reservasi ke :</li>
                        <ul>
                            <li>WA 0<?= Yii::$app->params[ 'waConfirm' ] ?> </li>
                            <li>Email <?= Yii::$app->params[ 'emailConfirm' ] ?></li>
                        </ul>
                        <li>Setelah bukti transfer diterima, tiket dikirim ke email anda</li>
                        <li>Paling lambat anda harus sudah tiba 15 menit sebelum waktu tour. Jika anda datang tidak sesuai dengan waktu maka akan dijadwal ualng sesuai kuota waktu yang tersedia</li>
                        </ol>
                        </p>
                    </div>
                    <div id="langsung" class="prosedurDesc" style="display: none;">
                        <p>
                            PROSEDUR PEMESANAN TIKET VIA LOKET
                        <ol>
                            <li>Kami menerima kunjungan lansung (belum reservasi) maksimal 10 orang</li>
                            <li>Harus mengisi inform consent</li>
                            <li>Pilih jenis tour</li>
                            <li>Waktu tour mengikuti kuota yang tersedia</li>
                            <li>Pembayaran via :</li>
                        </ol>
                        </p>
                    </div>
                    <div id="kunjungan" class="prosedurDesc" style="display: none;">
                        <p>
                            PROSEDUR KUNJUNGAN
                        <ol>
                            <li>Waktu tour adalah 80 menit.</li>
                            <li>Selama Tour, anda dipandu melalui audio guide dan arah panah</li>
                            <li>Setiap berada di suatu Tour Spot area, anda akan berada di suatu zona sejarah peradaban dan budaya melalui koleksi dan narasi audio guide.</li>
                            <li>Anda mengikuti panduan perpindahan tour spot area sesuai arahan dari audio guide</li>
                            <li>Anda harus menunggu pengunjung didepan anda maju, baru anda bisa melanjutkan ke tour spot area berikutnya</li>
                            <li>Jika anda mengalami gangguan tehnik, silahkan menghubungi petugas Museum di ruang bersangkutan</li>
                            <li>Selesai tour, audio guide dikembalikan pada tempat yang sudah ditentukan, pada akhir tour.</li>
                            <li>Jika anda memutuskan menghentikan sebelum tour selesai maka silahkan langsung menuju pintu exit dan menghubungi petugas.</li>
                        </ol>
                        </p>
                    </div>
                    <p>
                        Selama pandemi COVID-19, <strong>pengunjung usia anak-anak (<= 12 tahun) dan lansia (>= 65 tahun) serta ibu hamil </strong>belum diperkenankan berkunjung<br>
                        Batasan jumlah pengunjung rombongan maksimal 12 orang dengan waktu jeda tour 10 menit.
                    </p>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-7-datepicker">
                    <br>
                    <h2 class="subtitle"><u>Jenis Tur:</u></h2>
                    <div class="people-select-box">
                        <div>
                            <div class="select-style jenis-tour" style="width: 270px;">
								<?= Html::hiddenInput( 'people-select', 1 ); ?>
								<?= Html::dropDownList( 'tour-select', $tourSelect, [
									'Adiluhung Mataram' => 'Adiluhung Mataram',
									'Vorstenlanden'     => 'Vorstenlanden'
								], [ 'id' => 'tour-select' ] ) ?>
                            </div>
                            <br>
                            <div id="descriptionTour" style="display: block">
                                <p>
								<?
								switch ( $tourSelect ) {
									case 'Adiluhung Mataram':
										echo 'Area Guwo Selo Giri dan Kampung Kambang';
										break;
									case 'Vorstenlanden' :
										echo 'Area Jagad Galeri, Esther Huis, Sasono Sekar Bawono ... Menyusul .. akan dibuka segera ..';
										break;
								}
								?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <br>
                    <h2 class="subtitle"><u>Tanggal Kunjungan:</u></h2>
                    <div class="form-datepicker notranslate">
                        <!-- datepicker -->
                        <div id="datepicker" class="notranslate">
                        </div>
                        <input type="hidden" name="ticketTime" id="ticketTime" value="14:15">
                        <input type="hidden" name="overflowTime" id="overflowTime" value="">
                        <!-- datepicker for mobile -->
                        <div class="hidden notranslate">
                            <div class="date-group">
                                <input name="ticketDate" id="ticketDate" type="date" class="date-input notranslate" value="<?= $ticketDate; ?>">
                            </div>
                        </div>
                    </div>
                    <div class="date-soldout">
                        <div class="ui-state-disabled soldout">23</div>
                        = sold out
                    </div>
                    <div class="time-selection" style="display: block;">
                        <div class="loading" style="display: none;">
                            <p>
                                <strong>Times will be loaded...</strong>
                            </p>
                            <img src="img/loading.gif" alt="Loading">
                        </div>
                        <div class="timeslots" style="display: none;">
                            <h2 class="subtitle"><u>Jam Tur:</u></h2>
                            <div class="reference-container">
										<span class="item">
											<span class="color-block green"></span>
											<span>= Tickets available</span>
										</span>
                                <span class="item">
											<span class="color-block orange"></span>
											<span>= Some tickets still available</span>
										</span>
                                <span class="item">
											<span class="color-block gray"></span>
											<span>= No more tickets available</span>
										</span>
                            </div>
                            <div class="table-time"></div>
                            <div class="overflow-container" style="display:none;">
                                <p>
                                    <strong>Your date/timeslot of preference sold out?</strong><br>You can still visit our continuous exhibition on these timeslots: </p>
                            </div>
                            <div class="table-time-overflow" style="display: none;"></div>
                            <br>
                            <p>Tiket yang sudah dibeli tidak dapat di-refund.</p>
                            <div class="ticket-button-submit">
                                <a class="button btn-arrow-right btn-sec ticket-button-submit">
                                    <span class="buttonText">LANJUT</span> <span class="icons icon-arrow-right"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</form>