<?php
/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = 'Book your tickets - Ullen Sentalu Museum';
$session     = Yii::$app->session;
$session->open();
$this->registerJsVar( 'sisaSlot', $sisa );
$this->registerJsVar( 'perSlot', Yii::$app->params[ 'timeslot' ][ 'perSlot' ] );
$this->registerJsVar( 'allowChild', Yii::$app->params[ 'allowChild' ] );
?>
    <script>
        var language = 'en';
        var jsDomain = '';
        var ticketDateMin = new Date(2020, 6, 19);
        var ticketDateMax = new Date(2020, 8, 30);
        var disabledDays = [];
        var disabledDates = [];
        var soldoutDays = [""];
        var decimalSep = '.';
        var multimediaTourSave = {};
        var LANG_MULTIMEDIATOUR = 'Multimedia guide';
        var LANG_MULTIMEDIATOURVIDEO = 'nMnD129P-xk';
    </script>
    <form id="gt" method="post" action="<?= url::toRoute( [ 'site/checkout' ] ) ?>">
        <input id="form-token" type="hidden" name="<?= Yii::$app->request->csrfParam ?>"
               value="<?= Yii::$app->request->csrfToken ?>"/>
        <!--	--><? //= $this->render( '_nav-bottom', [
		//		'step' => 2,
		//		'buttonText' => '<span class="visible-xs-inline">3. </span> <span class="buttonText">Your information</span>'
		//	] ) ?>
        <section class="ticket-zone">
            <div class="container">
				<?php if ( Yii::$app->session->hasFlash( 'error' ) ): ?>
                    <div class="alert alert-danger alert-dismissable">
                        <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                        <h4><i class="icon fa fa-check"></i>Ooopppss something wrong</h4>
						<?= Yii::$app->session->getFlash( 'error' ) ?>
                    </div>
				<?php endif; ?>
                <div class="row">
                    <div class="col-sm-12 col-md-7">
                        <h1 class="subtitle">TIKET DAN DATA IDENTITAS</h1>
                        <div class="tickets">
                            <div class="form-persons">
                                <div class="row">
                                    <div class="ticket__header cf">
                                        <div class="col-md-12">
                                            <h3>Tur <?= $tourSelect; ?> <a href="<?= url::toRoute( [ 'site/index' ] ) ?>" class="change-link">Change</a></h3>
                                        </div>
                                    </div>
                                </div>
                                <table style="width: 100%;">
                                    <tbody>
                                    <tr>
                                        <td style="width: 50%;">Tanggal
                                            <br>
                                        </td>
                                        <td style="width: 50%;">: <?= $ticketDate; ?>
                                            <br>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 50%;">Jam Tur
                                            <br>
                                        </td>
                                        <td style="width: 50%;">: <?= $ticketTime; ?></td>
                                    </tr>
                                    <!--                                <tr>-->
                                    <!--                                    <td style="width: 50%;">Jumlah Tiket-->
                                    <!--                                        <br>-->
                                    <!--                                    </td>-->
                                    <!--                                    <td style="width: 50%;">: --><? //=$peopleSelect;?>
                                    <!--                                        <br>-->
                                    <!--                                    </td>-->
                                    <!--                                </tr>-->
                                    </tbody>
                                </table>
                                <div style="margin-bottom: 25px"></div>
                                <div class="row">
                                    <div class="ticket__header cf">
                                        <div class="col-md-12">
                                            <h3>Pilih Kategori Tiket</h3>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-ticketType" id="row_ra" data-mmt="adult">
                                    <div class="ticket__cat cf">
                                        <div class="col-xs-5 col-sm-7">
                                            <p class="pull-left ticket-description" data-description="adult">Dewasa</p>
                                            <p class="pull-right ticket-price" data-price="<?= $hargaDewasa ?>">Rp <?= number_format( $hargaDewasa, 0, ',', '.' ) ?></p>
                                        </div>
                                        <div class="col-xs-2">
                                            <div class="select-style">
                                                <select name="qtyDewasa" id="tickets_ra" class="tickets-selection" data-begeleidermmt="true">
                                                    <option value="0">0</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-5 col-sm-3">
                                            <p class="text--right">
                                                <span class="euro" style="display: none;">Rp </span>
                                                <span class="total" style="display: none;">&nbsp;</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="row row-ticketType" id="row_rc" data-mmt="child" >
                                    <div class="ticket__cat cf">
                                        <div class="col-xs-5 col-sm-7">
                                            <p class="pull-left ticket-description" data-description="child">Anak (5-12 th)</p>
                                            <p class="pull-right ticket-price" data-price="<?= $hargaAnak ?>">Rp <?= number_format( $hargaAnak, 0, ',', '.' ) ?></p>
                                        </div>
                                        <div class="col-xs-2">
                                            <div class="select-style">
                                                <select name="qtyAnak" id="tickets_rc" class="tickets-selection" data-begeleidermmt="true">
                                                    <option value="0">0</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xs-5 col-sm-3">
                                            <p class="text--right">
                                                <span class="euro" style="display: none;">Rp </span>
                                                <span class="total" style="display: none;">&nbsp;</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-xs-6 text--right">
                                    <a class="hidden" id="add-tickets" data-ticketsamount="0" data-ticketstotal="0" data-multimediatourtotal="0">0</a>
                                </div>
                                <div id="tickets-part2">
                                    <div class="row">
                                        <div class="ticket__subtotal cf">
                                            <div class="col-xs-7">
                                                <p>Total</p>
                                            </div>
                                            <div class="col-xs-5">
                                                <p class="text--right" id="total">
                                                    <span class="euro">Rp </span>
                                                    <span class="total">0.00</span>
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-ticket">
                                    <div class="row">
                                        <div class="ticket__header cf">
                                            <div class="col-md-12">
                                                <h3>Data Identitas</h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label for="firstName">Nama Lengkap</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input required type="text" id="firstName" name="completeName" class="ticket-input" value="">
                                        </div>
                                    </div> <!-- row -->
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label for="handphone">No. HP (WA)</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input required type="text" id="handphone" name="handphone" class="ticket-input" value="">
                                        </div>
                                    </div> <!-- row -->
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label for="emailAddress">Email</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input required type="text" id="emailAddress" name="emailAddress" class="ticket-input" value="">
                                        </div>
                                    </div> <!-- row -->
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label for="emailAddressConfirm">Konfirm email</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input required type="text" id="emailAddressConfirm" name="emailAddressConfirm" class="ticket-input" value="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label for="city">Kota</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <input required type="text" id="city" name="city" class="ticket-input" value="">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <label for="country">Negara</label>
                                        </div>
                                        <div class="col-sm-8">
                                            <div class="select-style-sec">
                                                <select id="country" name="country">
                                                    <option value="IT">Italy</option>
                                                    <option value="FR">France</option>
                                                    <option value="DE">Germany</option>
                                                    <option value="GB">United Kingdom</option>
                                                    <option value="ES">Spain</option>
                                                    <option value="US">United States Of America</option>
                                                    <option value="BR">Brazil</option>
                                                    <option value="CH">Switzerland</option>
                                                    <option value="JP">Japan</option>
                                                    <option value="NL">Netherlands</option>
                                                    <option value="BE">Belgium</option>
                                                    <option value="" disabled="disabled">---------------------------------</option>
                                                    <option value="AF">Afghanistan</option>
                                                    <option value="AX">Åland Islands</option>
                                                    <option value="AL">Albania</option>
                                                    <option value="DZ">Algeria</option>
                                                    <option value="AS">American Samoa</option>
                                                    <option value="AD">Andorra</option>
                                                    <option value="AO">Angola</option>
                                                    <option value="AI">Anguilla</option>
                                                    <option value="AQ">Antarctica</option>
                                                    <option value="AG">Antigua And Barbuda</option>
                                                    <option value="AR">Argentina</option>
                                                    <option value="AM">Armenia</option>
                                                    <option value="AW">Aruba</option>
                                                    <option value="AU">Australia</option>
                                                    <option value="AT">Austria</option>
                                                    <option value="AZ">Azerbaijan</option>
                                                    <option value="BS">Bahamas</option>
                                                    <option value="BH">Bahrain</option>
                                                    <option value="BD">Bangladesh</option>
                                                    <option value="BB">Barbados</option>
                                                    <option value="BY">Belarus</option>
                                                    <option value="BE">Belgium</option>
                                                    <option value="BZ">Belize</option>
                                                    <option value="BJ">Benin</option>
                                                    <option value="BM">Bermuda</option>
                                                    <option value="BT">Bhutan</option>
                                                    <option value="BO">Bolivia</option>
                                                    <option value="BQ">Bonaire, Sint Eustatius And Saba</option>
                                                    <option value="BA">Bosnia And Herzegowina</option>
                                                    <option value="BW">Botswana</option>
                                                    <option value="BV">Bouvet Island</option>
                                                    <option value="BR">Brazil</option>
                                                    <option value="IO">British Indian Ocean Territory</option>
                                                    <option value="BN">Brunei Darussalam</option>
                                                    <option value="BG">Bulgaria</option>
                                                    <option value="BF">Burkina Faso</option>
                                                    <option value="BI">Burundi</option>
                                                    <option value="KH">Cambodia</option>
                                                    <option value="CM">Cameroon</option>
                                                    <option value="CA">Canada</option>
                                                    <option value="CV">Cape Verde</option>
                                                    <option value="KY">Cayman Islands</option>
                                                    <option value="CF">Central African Republic</option>
                                                    <option value="TD">Chad</option>
                                                    <option value="CL">Chile</option>
                                                    <option value="CN">China</option>
                                                    <option value="CX">Christmas Island</option>
                                                    <option value="CC">Cocos (Keeling) Islands</option>
                                                    <option value="CO">Colombia</option>
                                                    <option value="KM">Comoros</option>
                                                    <option value="CG">Congo-Brazzaville</option>
                                                    <option value="CD">Congo-Kinshasa</option>
                                                    <option value="CK">Cook Islands</option>
                                                    <option value="CR">Costa Rica</option>
                                                    <option value="CI">Cote D'Ivoire</option>
                                                    <option value="HR">Croatia</option>
                                                    <option value="CU">Cuba</option>
                                                    <option value="CW">Curacao !Curaçao</option>
                                                    <option value="CY">Cyprus</option>
                                                    <option value="CZ">Czech Republic</option>
                                                    <option value="DK">Denmark</option>
                                                    <option value="DJ">Djibouti</option>
                                                    <option value="DM">Dominica</option>
                                                    <option value="DO">Dominican Republic</option>
                                                    <option value="TL">East Timor</option>
                                                    <option value="EC">Ecuador</option>
                                                    <option value="EG">Egypt</option>
                                                    <option value="SV">El Salvador</option>
                                                    <option value="GQ">Equatorial Guinea</option>
                                                    <option value="ER">Eritrea</option>
                                                    <option value="EE">Estonia</option>
                                                    <option value="ET">Ethiopia</option>
                                                    <option value="FK">Falkland Islands (Malvinas)</option>
                                                    <option value="FO">Faroe Islands</option>
                                                    <option value="FJ">Fiji</option>
                                                    <option value="FI">Finland</option>
                                                    <option value="FR">France</option>
                                                    <option value="GF">French Guiana</option>
                                                    <option value="PF">French Polynesia</option>
                                                    <option value="TF">French Southern Territories</option>
                                                    <option value="GA">Gabon</option>
                                                    <option value="GM">Gambia</option>
                                                    <option value="GE">Georgia</option>
                                                    <option value="DE">Germany</option>
                                                    <option value="GH">Ghana</option>
                                                    <option value="GI">Gibraltar</option>
                                                    <option value="GR">Greece</option>
                                                    <option value="GL">Greenland</option>
                                                    <option value="GD">Grenada</option>
                                                    <option value="GP">Guadeloupe</option>
                                                    <option value="GU">Guam</option>
                                                    <option value="GT">Guatemala</option>
                                                    <option value="GG">Guernsey</option>
                                                    <option value="GN">Guinea</option>
                                                    <option value="GW">Guinea-Bissau</option>
                                                    <option value="GY">Guyana</option>
                                                    <option value="HT">Haiti</option>
                                                    <option value="HM">Heard Island And Mcdonald Islands</option>
                                                    <option value="HN">Honduras</option>
                                                    <option value="HK">Hong Kong</option>
                                                    <option value="HU">Hungary</option>
                                                    <option value="IS">Iceland</option>
                                                    <option value="IN">India</option>
                                                    <option value="ID" selected>Indonesia</option>
                                                    <option value="IR">Iran</option>
                                                    <option value="IQ">Iraq</option>
                                                    <option value="IE">Ireland</option>
                                                    <option value="IM">Isle Of Man</option>
                                                    <option value="IL">Israel</option>
                                                    <option value="IT">Italy</option>
                                                    <option value="JM">Jamaica</option>
                                                    <option value="JP">Japan</option>
                                                    <option value="JE">Jersey</option>
                                                    <option value="JO">Jordan</option>
                                                    <option value="KZ">Kazakhstan</option>
                                                    <option value="KE">Kenya</option>
                                                    <option value="KI">Kiribati</option>
                                                    <option value="KP">Korea, Democratic People's Republic Of</option>
                                                    <option value="KR">Korea, South</option>
                                                    <option value="KO">Kosovo</option>
                                                    <option value="KW">Kuwait</option>
                                                    <option value="KG">Kyrgyzstan</option>
                                                    <option value="LA">Lao People's Democratic Republic</option>
                                                    <option value="LV">Latvia</option>
                                                    <option value="LB">Lebanon</option>
                                                    <option value="LS">Lesotho</option>
                                                    <option value="LR">Liberia</option>
                                                    <option value="LY">Libya</option>
                                                    <option value="LI">Liechtenstein</option>
                                                    <option value="LT">Lithuania</option>
                                                    <option value="LU">Luxembourg</option>
                                                    <option value="MO">Macao</option>
                                                    <option value="MK">Macedonia, The Former Yugoslav Republic</option>
                                                    <option value="MG">Madagascar</option>
                                                    <option value="MW">Malawi</option>
                                                    <option value="MY">Malaysia</option>
                                                    <option value="MV">Maldives</option>
                                                    <option value="ML">Mali</option>
                                                    <option value="MT">Malta</option>
                                                    <option value="MH">Marshall Islands</option>
                                                    <option value="MQ">Martinique</option>
                                                    <option value="MR">Mauritania</option>
                                                    <option value="MU">Mauritius</option>
                                                    <option value="YT">Mayotte</option>
                                                    <option value="MX">Mexico</option>
                                                    <option value="FM">Micronesia, Federated States Of</option>
                                                    <option value="MD">Moldova, Republic Of</option>
                                                    <option value="MC">Monaco</option>
                                                    <option value="MN">Mongolia</option>
                                                    <option value="ME">Montenegro</option>
                                                    <option value="MS">Montserrat</option>
                                                    <option value="MA">Morocco</option>
                                                    <option value="MZ">Mozambique</option>
                                                    <option value="MM">Myanmar</option>
                                                    <option value="NA">Namibia</option>
                                                    <option value="NR">Nauru</option>
                                                    <option value="NP">Nepal</option>
                                                    <option value="NL">Netherlands</option>
                                                    <option value="NC">New Caledonia</option>
                                                    <option value="NZ">New Zealand</option>
                                                    <option value="NI">Nicaragua</option>
                                                    <option value="NE">Niger</option>
                                                    <option value="NG">Nigeria</option>
                                                    <option value="NU">Niue</option>
                                                    <option value="NF">Norfolk Island</option>
                                                    <option value="MP">Northern Mariana Islands</option>
                                                    <option value="NO">Norway</option>
                                                    <option value="OM">Oman</option>
                                                    <option value="PK">Pakistan</option>
                                                    <option value="PW">Palau</option>
                                                    <option value="PS">Palestine, State Of</option>
                                                    <option value="PA">Panama</option>
                                                    <option value="PG">Papua New Guinea</option>
                                                    <option value="PY">Paraguay</option>
                                                    <option value="PE">Peru</option>
                                                    <option value="PH">Philippines</option>
                                                    <option value="PN">Pitcairn</option>
                                                    <option value="PL">Poland</option>
                                                    <option value="PT">Portugal</option>
                                                    <option value="PR">Puerto Rico</option>
                                                    <option value="QA">Qatar</option>
                                                    <option value="RE">Reunion !Réunion</option>
                                                    <option value="RO">Romania</option>
                                                    <option value="RU">Russia</option>
                                                    <option value="RW">Rwanda</option>
                                                    <option value="BL">Saint Barthelemy !Saint Barthélemy</option>
                                                    <option value="SH">Saint Helena, Ascension And Tristan Da Cunha</option>
                                                    <option value="MF">Saint Martin (French Part)</option>
                                                    <option value="PM">Saint Pierre And Miquelon</option>
                                                    <option value="WS">Samoa</option>
                                                    <option value="SM">San Marino</option>
                                                    <option value="ST">Sao Tome And Principe</option>
                                                    <option value="SA">Saudi Arabia</option>
                                                    <option value="SN">Senegal</option>
                                                    <option value="RS">Serbia</option>
                                                    <option value="SC">Seychelles</option>
                                                    <option value="SL">Sierra Leone</option>
                                                    <option value="SG">Singapore</option>
                                                    <option value="SX">Sint-Maarten (Dutch Part)</option>
                                                    <option value="SK">Slovakia</option>
                                                    <option value="SI">Slovenia</option>
                                                    <option value="SB">Solomon Islands</option>
                                                    <option value="SO">Somalia</option>
                                                    <option value="ZA">South Africa</option>
                                                    <option value="GS">South Georgia And The South Sandwich Islands</option>
                                                    <option value="SS">South Sudan</option>
                                                    <option value="ES">Spain</option>
                                                    <option value="LK">Sri Lanka</option>
                                                    <option value="KN">St. Kitts And Nevis</option>
                                                    <option value="LC">St. Lucia</option>
                                                    <option value="VC">St. Vincent And The Grenadines</option>
                                                    <option value="SD">Sudan</option>
                                                    <option value="SR">Suriname</option>
                                                    <option value="SJ">Svalbard And Jan Mayen</option>
                                                    <option value="SZ">Swaziland</option>
                                                    <option value="SE">Sweden</option>
                                                    <option value="CH">Switzerland</option>
                                                    <option value="SY">Syria</option>
                                                    <option value="TW">Taiwan</option>
                                                    <option value="TJ">Tajikistan</option>
                                                    <option value="TZ">Tanzania</option>
                                                    <option value="TH">Thailand</option>
                                                    <option value="TG">Togo</option>
                                                    <option value="TK">Tokelau</option>
                                                    <option value="TO">Tonga</option>
                                                    <option value="TT">Trinidad And Tobago</option>
                                                    <option value="TN">Tunisia</option>
                                                    <option value="TR">Turkey</option>
                                                    <option value="TM">Turkmenistan</option>
                                                    <option value="TC">Turks And Caicos Islands</option>
                                                    <option value="TV">Tuvalu</option>
                                                    <option value="UG">Uganda</option>
                                                    <option value="UA">Ukraine</option>
                                                    <option value="AE">United Arab Emirates</option>
                                                    <option value="GB">United Kingdom</option>
                                                    <option value="UM">United States Minor Outlying Islands</option>
                                                    <option value="US">United States Of America</option>
                                                    <option value="UY">Uruguay</option>
                                                    <option value="UZ">Uzbekistan</option>
                                                    <option value="VU">Vanuatu</option>
                                                    <option value="VA">Vatican City (Holy See)</option>
                                                    <option value="VE">Venezuela</option>
                                                    <option value="VN">Vietnam</option>
                                                    <option value="VG">Virgin Islands (British)</option>
                                                    <option value="VI">Virgin Islands (U.S.)</option>
                                                    <option value="WF">Wallis And Futuna</option>
                                                    <option value="EH">Western Sahara</option>
                                                    <option value="YE">Yemen</option>
                                                    <option value="ZM">Zambia</option>
                                                    <option value="ZW">Zimbabwe</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div id="tickets-part2">
                                    <div class="row">
                                        <div class="ticket-button-submit active">
                                            <a class="button btn-arrow-right btn-sec ticket-button-submit">
                                                <span class="buttonText">LANJUT</span><span class="buttonIamsterdam"
                                                                                                                                       style="display:none;">Your information</span>
                                                <span class="icons icon-arrow-right"></span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- row -->
            </div> <!-- container -->
        </section> <!-- ticket-zone -->
    </form>
<?php
$this->registerJs( <<<JS

var maxTiket = sisaSlot * perSlot;
$('#tickets_ra').change(function () {
    if (!allowChild) return;
    var sisa = maxTiket - parseInt(this.value);
    var c = $('#tickets_rc').val();
    if (sisa < c) c = 0;
    $('#tickets_rc').empty();
    var i;
    for (i = 0; i <= sisa; i++) {
        $('#tickets_rc').append('<option value="' + i + '">' + i + '</option>');
    }
    $('#tickets_rc').val(c);
})
$('#tickets_rc').change(function () {
    var sisa = maxTiket - parseInt(this.value);
        var c = $('#tickets_ra').val();
        if (sisa < c) c = 0;
        $('#tickets_ra').empty();
        var i;
        for (i = 0; i <= sisa; i++) {
            $('#tickets_ra').append('<option  value="' + i + '">' + i + '</option>');
        }
        $('#tickets_ra').val(c);
})
var i;
for (i = 1; i <= maxTiket; i++) {
    $('#tickets_ra').append('<option  value="' + i + '">' + i + '</option>');
    if (allowChild) $('#tickets_rc').append('<option  value="' + i + '">' + i + '</option>');
}

$('#gt').submit(function(e) {
    if (parseInt($('#total').val()) === 0){
        alert('Tiket harus dipilih');
        e.preventDefault();
        return;
    }
    if ($('#firstName').val() === ''){
        alert('Nama harus diisi');
        e.preventDefault();
         return;
    }
    if ($('#handphone').val() === ''){
        alert('No Hp harus diisi');
        e.preventDefault();
         return;
    }
    if ($('#emailAddress').val() === ''){
        alert('Email harus diisi');
        e.preventDefault();
         return;
    }
    if ($('#city').val() === ''){
        alert('Kota harus diisi');
        e.preventDefault();
         return;
    }
})

JS
)
?>
<?php $session->close();