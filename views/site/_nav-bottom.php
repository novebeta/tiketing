<?php
use yii\helpers\Url;
$session = Yii::$app->session;
$session->open();
$isPage1Done = $session->has('page_1');
$isPage2Done = $session->has('page_2');
$isPage3Done = $session->has('page_3');
$isPage4Done = $session->has('page_4');
?>
<input id="form-token" type="hidden" name="<?=Yii::$app->request->csrfParam?>"
       value="<?=Yii::$app->request->csrfToken?>"/>
<nav class="nav-steps fixed-bottom fixed-bottom-extended">
	<div class="container">
		<div class="zone">
			<div class="col-xs-12 col-sm-9 steps">
				<div class="step <?=($step == 1? 'active':'')?> <?=($step != 1 && $isPage1Done? 'done':'')?>" id="navDateTime">
					<a href="<?=url::toRoute(['site/index'])?>">
						<p>1. <span class="hidden-xs">Plan your visit</span></p>
						<p class="sub hidden-xs" style="display: none; height: 34px; padding-top: 0px; margin-top: 15px; padding-bottom: 0px; margin-bottom: 15px;">
							<span id="navDate"><?=($isPage1Done ? date('d F', strtotime($session->get('page_1')['ticketDate'])) : '') ?></span>
							<span id="navTime"><?=($isPage1Done ? $session->get('page_1')['ticketTime'] : '') ?></span>
						</p>
					</a>
				</div>
				<div class="step <?=($step == 2? 'active':'')?> <?=($step != 2 && $isPage2Done? 'done':'')?>" id="navTicketsSummary">
                    <? if($isPage2Done) : ?><a href="<?=url::toRoute(['site/tiket'])?>"><?endif;?>
					<p>2. <span class="hidden-xs">Tickets</span></p>
                    <? if($isPage2Done) : ?>
                    <p class="sub hidden-xs" style="display: none; height: 119px; padding-top: 0px; margin-top: 15px; padding-bottom: 0px; margin-bottom: 15px;">
					<span id="navTickets">
                        <?=($session->get('page_2')['tickets']['ra'] > 0) ? $session->get('page_2')['tickets']['ra'].'x Adiluhung Mataram Adult<br>' : '' ?>
                        <?=($session->get('page_2')['tickets']['rc'] > 0) ? $session->get('page_2')['tickets']['rc'].'x Adiluhung Mataram Child<br>' : '' ?>
                        <?=($session->get('page_2')['tickets']['va'] > 0) ? $session->get('page_2')['tickets']['va'].'x Vorstenlanden Adult<br>' : '' ?>
                        <?=($session->get('page_2')['tickets']['vc'] > 0) ? $session->get('page_2')['tickets']['vc'].'x Vorstenlanden Child<br>' : '' ?>
                    </span>
                    </p>
                    <? endif; ?>
                    <? if($isPage2Done) : ?></a><?endif;?>
				</div>
				<div class="step <?=($step == 3? 'active':'')?> <?=($step != 3 && $isPage3Done? 'done':'')?>" id="navYourInformation">
                    <? if($isPage3Done) : ?><a href="<?=url::toRoute(['site/your-information'])?>"><?endif;?>
					<p>3. <span class="hidden-xs">Your information</span></p>
                    <p class="sub hidden-xs" style="display: none; height: 119px; padding-top: 0px; margin-top: 15px; padding-bottom: 0px; margin-bottom: 15px;">
					<span class="footer_mail">
                        <?=($isPage3Done) ? $session->get('page_3')['firstName'].' '.$session->get('page_3')['surName'].'<br>' : '' ?>
                        <?=($isPage3Done) ? $session->get('page_3')['emailAddressConfirm'] : '' ?>
                    </span>
                    </p>
					<? if($isPage3Done) : ?></a><?endif;?>
				</div>
				<div class="step <?=($step == 4? 'active':'')?> <?=($step != 4 && $isPage4Done? 'done':'')?>" id="navPayment">
                    <a href="<?=url::toRoute(['site/check-your-order'])?>">
					<p>4. <span class="hidden-xs">Payment</span></p>
                    </a>
				</div>
			</div>
            <? if($buttonText != '') : ?>
			<div class="col-xs-12 col-sm-3 ticket-button" style="">
				<a class="button btn-arrow-right btn-sec">
					<?=$buttonText;?>
                    <span class="icons icon-arrow-right"></span>
				</a>
			</div>
            <? endif; ?>
		</div>
	</div>
</nav>
<?php $session->close();