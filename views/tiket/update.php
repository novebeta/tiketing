<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Tiket */

$this->title = 'Update Reservasi: ' . $model->no_booking;
$this->params['breadcrumbs'][] = ['label' => 'Tikets', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->no_booking, 'url' => ['view', 'id' => $model->tiket_id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="tiket-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
