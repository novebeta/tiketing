<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Tiket */

$this->title = $model->tiket_id;
$this->params['breadcrumbs'][] = ['label' => 'Tikets', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="tiket-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->tiket_id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->tiket_id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'tiket_id',
            'tdate',
            'jenis_tur',
            'jml',
            'tiket_tgl',
            'tiket_jam',
            'dewasa',
            'anak',
            'dewasa_harga',
            'anak_harga',
            'total',
            'nama_lengkap',
            'no_hp',
            'email:email',
            'kota',
            'negara',
            'expired',
            'no_invoice',
            'transaksi_no',
            'transaksi_waktu',
            'transaksi_bank',
            'transaksi_pengirim',
        ],
    ]) ?>

</div>
