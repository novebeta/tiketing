<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
/* @var $this yii\web\View */
/* @var $model app\models\Tiket */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="tiket-form">
	<?php $form = ActiveForm::begin(); ?>


	<?= $form->field( $model, 'tdate' )->textInput( [ 'readOnly' => true ] ) ?>

	<?= $form->field( $model, 'jenis_tur' )->textInput( [ 'maxlength' => true, 'readOnly' => true ] ) ?>

	<?= $form->field( $model, 'jml' )->textInput( [ 'readOnly' => true ] ) ?>

	<?= $form->field( $model, 'tiket_tgl' )->textInput( [ 'readOnly' => true ] ) ?>

	<?= $form->field( $model, 'tiket_jam' )->textInput( [ 'readOnly' => true ] ) ?>

	<?= $form->field( $model, 'dewasa' )->textInput( [ 'readOnly' => true ] ) ?>

	<?= $form->field( $model, 'anak' )->textInput( [ 'readOnly' => true ] ) ?>

	<?= $form->field( $model, 'dewasa_harga' )->textInput( [ 'maxlength' => true, 'readOnly' => true ] ) ?>

	<?= $form->field( $model, 'anak_harga' )->textInput( [ 'maxlength' => true, 'readOnly' => true ] ) ?>

	<?= $form->field( $model, 'total' )->textInput( [ 'maxlength' => true, 'readOnly' => true ] ) ?>

	<?= $form->field( $model, 'nama_lengkap' )->textInput( [ 'maxlength' => true, 'readOnly' => true ] ) ?>

	<?= $form->field( $model, 'no_hp' )->textInput( [ 'maxlength' => true, 'readOnly' => true ] ) ?>

	<?= $form->field( $model, 'email' )->textInput( [ 'maxlength' => true, 'readOnly' => true ] ) ?>

	<?= $form->field( $model, 'kota' )->textInput( [ 'maxlength' => true, 'readOnly' => true ] ) ?>

	<?= $form->field( $model, 'negara' )->textInput( [ 'maxlength' => true, 'readOnly' => true ] ) ?>

	<?= $form->field( $model, 'expired' )->textInput( [ 'readOnly' => true, ] ) ?>

	<?= $form->field( $model, 'no_invoice' )->textInput( [ 'maxlength' => true, 'readOnly' => true ] ) ?>

	<?= $form->field( $model, 'transaksi_no' )->textInput( [ 'maxlength' => true ] ) ?>

	<?= $form->field( $model, 'transaksi_bank' )->textInput( [ 'maxlength' => true ] ) ?>

	<?= $form->field( $model, 'transaksi_pengirim' )->textInput( [ 'maxlength' => true ] ) ?>
	<?= $form->field( $model, 'transaksi_tiket' )->textarea() ?>
    <div class="form-group">
		<?= Html::submitButton( 'Save', [ 'class' => 'btn btn-success' ] ) ?>
    </div>
	<?php ActiveForm::end(); ?>
</div>
