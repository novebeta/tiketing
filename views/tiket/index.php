<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel app\models\TiketSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Tikets';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="tiket-index">

<!--    <h1>--><?//= Html::encode($this->title) ?><!--</h1>-->

    <p>
<!--        --><?//= Html::a('Create Tiket', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'no_booking',
            'nama_lengkap',
            'no_hp',
            'email:email',
            'jenis_tur',
            //            'tdate',
            'tiket_tgl',
            'tiket_jam',
            'jml',
            'total',
            //'dewasa',
            //'anak',
            //'dewasa_harga',
            //'anak_harga',
//            'kota',
//            'negara',
//            'expired',
            'no_invoice',
//            'transaksi_no',
//            'transaksi_waktu',
//            'transaksi_bank',
//            'transaksi_pengirim',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
