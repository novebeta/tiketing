<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TiketSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="tiket-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
        'options' => [
            'data-pjax' => 1
        ],
    ]); ?>

    <?= $form->field($model, 'tiket_id') ?>

    <?= $form->field($model, 'tdate') ?>

    <?= $form->field($model, 'jenis_tur') ?>

    <?= $form->field($model, 'jml') ?>

    <?= $form->field($model, 'tiket_tgl') ?>

    <?php // echo $form->field($model, 'tiket_jam') ?>

    <?php // echo $form->field($model, 'dewasa') ?>

    <?php // echo $form->field($model, 'anak') ?>

    <?php // echo $form->field($model, 'dewasa_harga') ?>

    <?php // echo $form->field($model, 'anak_harga') ?>

    <?php // echo $form->field($model, 'total') ?>

    <?php // echo $form->field($model, 'nama_lengkap') ?>

    <?php // echo $form->field($model, 'no_hp') ?>

    <?php // echo $form->field($model, 'email') ?>

    <?php // echo $form->field($model, 'kota') ?>

    <?php // echo $form->field($model, 'negara') ?>

    <?php // echo $form->field($model, 'expired') ?>

    <?php // echo $form->field($model, 'no_invoice') ?>

    <?php // echo $form->field($model, 'transaksi_no') ?>

    <?php // echo $form->field($model, 'transaksi_waktu') ?>

    <?php // echo $form->field($model, 'transaksi_bank') ?>

    <?php // echo $form->field($model, 'transaksi_pengirim') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
