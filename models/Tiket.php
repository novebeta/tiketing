<?php
namespace app\models;
use DateTime;
/**
 * This is the model class for table "tiket".
 *
 * @property string $tiket_id
 * @property string $tdate
 * @property string $jenis_tur
 * @property int $jml
 * @property string $tiket_tgl
 * @property string $tiket_jam
 * @property int $dewasa
 * @property int $anak
 * @property float $dewasa_harga
 * @property float $anak_harga
 * @property float $total
 * @property string $nama_lengkap
 * @property string $no_hp
 * @property string $email
 * @property string $kota
 * @property string $negara
 * @property string $expired
 * @property string|null $no_invoice
 * @property string|null $transaksi_no
 * @property string|null $transaksi_waktu
 * @property string|null $transaksi_bank
 * @property string|null $transaksi_pengirim
 * @property string $no_booking
 * @property string|null $transaksi_tiket
 */
class Tiket extends \yii\db\ActiveRecord {
	/**
	 * {@inheritdoc}
	 */
	public static function tableName() {
		return 'tiket';
	}
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'tiket_id', 'tdate', 'jenis_tur', 'jml', 'tiket_tgl', 'tiket_jam', 'dewasa', 'anak', 'dewasa_harga', 'anak_harga', 'total', 'nama_lengkap', 'no_hp', 'email', 'kota', 'negara', 'expired', 'no_booking' ], 'required' ],
			[ [ 'tdate', 'tiket_tgl', 'tiket_jam', 'expired', 'transaksi_waktu' ], 'safe' ],
			[ [ 'jml', 'dewasa', 'anak' ], 'integer' ],
			[ [ 'dewasa_harga', 'anak_harga', 'total' ], 'number' ],
			[ [ 'transaksi_tiket' ], 'string' ],
			[ [ 'tiket_id' ], 'string', 'max' => 36 ],
			[ [ 'jenis_tur', 'nama_lengkap', 'no_hp', 'email', 'kota', 'negara', 'transaksi_no', 'transaksi_bank', 'transaksi_pengirim' ], 'string', 'max' => 50 ],
			[ [ 'no_invoice' ], 'string', 'max' => 15 ],
			[ [ 'no_booking' ], 'string', 'max' => 10 ],
			[ [ 'tiket_id' ], 'unique' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function attributeLabels() {
		return [
			'tiket_id'           => 'SystemID',
			'tdate'              => 'Waktu Transaksi',
			'jenis_tur'          => 'Jenis Tur',
			'jml'                => 'Slot',
			'tiket_tgl'          => 'Reservasi Tanggal',
			'tiket_jam'          => 'Reservasi Jam',
			'dewasa'             => 'Jumlah Dewasa',
			'anak'               => 'Jumlah Anak',
			'dewasa_harga'       => 'Harga Dewasa',
			'anak_harga'         => 'Harga Anak',
			'total'              => 'Nilai Bayar',
			'nama_lengkap'       => 'Nama Lengkap',
			'no_hp'              => 'No Hp (WA)',
			'email'              => 'Email',
			'kota'               => 'Kota',
			'negara'             => 'Negara',
			'expired'            => 'Expired',
			'no_invoice'         => 'No Invoice',
			'transaksi_no'       => 'No Transfer',
			'transaksi_waktu'    => 'Transaksi Waktu',
			'transaksi_bank'     => 'Bank Transfer',
			'transaksi_pengirim' => 'Pengirim Transfer',
			'no_booking'         => 'No Reservasi',
			'transaksi_tiket'    => 'Tiket',
		];
	}
	/**
	 * {@inheritdoc}
	 * @return TiketQuery the active query used by this AR class.
	 */
	public static function find() {
		return new TiketQuery( get_called_class() );
	}
	public static function hoursRange( $lower = 0, $upper = 86400, $step = 3600, $format = '' ) {
		$times = [];
		if ( empty( $format ) ) {
			$format = 'g:i a';
		}
		foreach ( range( $lower, $upper, $step ) as $increment ) {
			$increment = gmdate( 'H:i', $increment );
			list( $hour, $minutes ) = explode( ':', $increment );
			try {
				$date                         = new DateTime( $hour . ':' . $minutes );
				$times[ (string) $increment ] = $date->format( $format );
			} catch ( \Exception $e ) {
			}
		}
		return $times;
	}
	public static function genInv($jenisTour){
		$kode = '';
		switch ($jenisTour){
			case 'Adiluhung Mataram':
				$kode = 'AM';
				break;
			case 'Vorstenlanden':
				$kode = 'VT';
				break;
		}
		$yNow    = date( "ym" );
		$maxNoGl = ( new \yii\db\Query() )
			->from( self::tableName() )
			->where( "no_invoice LIKE '$kode/$yNow/%'" )
			->max( 'no_invoice' );
		if ( $maxNoGl && preg_match( "/$kode\/\d{4}\/(\d+)/", $maxNoGl, $result ) ) {
			[ $all, $counter ] = $result;
			$digitCount  = strlen( $counter );
			$val         = (int) $counter;
			$nextCounter = sprintf( '%0' . $digitCount . 'd', ++ $val );
		} else {
			$nextCounter = "001";
		}
		return "$kode/$yNow/$nextCounter";
	}
}
