<?php
namespace app\models;
use yii\base\Model;
use yii\data\ActiveDataProvider;
/**
 * TiketSearch represents the model behind the search form of `app\models\Tiket`.
 */
class TiketSearch extends Tiket {
	/**
	 * {@inheritdoc}
	 */
	public function rules() {
		return [
			[ [ 'tiket_id', 'tdate', 'jenis_tur', 'tiket_tgl', 'tiket_jam', 'no_booking', 'nama_lengkap', 'no_hp', 'email', 'kota', 'negara', 'expired', 'no_invoice', 'transaksi_no', 'transaksi_waktu', 'transaksi_bank', 'transaksi_pengirim' ], 'safe' ],
			[ [ 'jml', 'dewasa', 'anak' ], 'integer' ],
			[ [ 'dewasa_harga', 'anak_harga', 'total' ], 'number' ],
		];
	}
	/**
	 * {@inheritdoc}
	 */
	public function scenarios() {
		// bypass scenarios() implementation in the parent class
		return Model::scenarios();
	}
	/**
	 * Creates data provider instance with search query applied
	 *
	 * @param array $params
	 *
	 * @return ActiveDataProvider
	 */
	public function search( $params ) {
		$query = Tiket::find();
		// add conditions that should always apply here
		$dataProvider = new ActiveDataProvider( [
			'query' => $query,
		] );
		$this->load( $params );
		if ( ! $this->validate() ) {
			// uncomment the following line if you do not want to return any records when validation fails
			// $query->where('0=1');
			return $dataProvider;
		}
		// grid filtering conditions
		$query->andFilterWhere( [
			'tdate'           => $this->tdate,
			'jml'             => $this->jml,
			'tiket_tgl'       => $this->tiket_tgl,
			'tiket_jam'       => $this->tiket_jam,
			'dewasa'          => $this->dewasa,
			'anak'            => $this->anak,
			'dewasa_harga'    => $this->dewasa_harga,
			'anak_harga'      => $this->anak_harga,
			'total'           => $this->total,
			'expired'         => $this->expired,
			'transaksi_waktu' => $this->transaksi_waktu,
		] );
		$query->andFilterWhere( [ '>', 'expired', $this->expired ] );
		$query->andFilterWhere( [ 'like', 'tiket_id', $this->tiket_id ] )
		      ->andFilterWhere( [ 'like', 'no_booking', $this->no_booking ] )
		      ->andFilterWhere( [ 'like', 'jenis_tur', $this->jenis_tur ] )
		      ->andFilterWhere( [ 'like', 'nama_lengkap', $this->nama_lengkap ] )
		      ->andFilterWhere( [ 'like', 'no_hp', $this->no_hp ] )
		      ->andFilterWhere( [ 'like', 'email', $this->email ] )
		      ->andFilterWhere( [ 'like', 'kota', $this->kota ] )
		      ->andFilterWhere( [ 'like', 'negara', $this->negara ] )
		      ->andFilterWhere( [ 'like', 'no_invoice', $this->no_invoice ] )
		      ->andFilterWhere( [ 'like', 'transaksi_no', $this->transaksi_no ] )
		      ->andFilterWhere( [ 'like', 'transaksi_bank', $this->transaksi_bank ] )
		      ->andFilterWhere( [ 'like', 'transaksi_pengirim', $this->transaksi_pengirim ] );
		return $dataProvider;
	}
}
