<?php
namespace app\models;
use Yii;
use yii\db\Exception;
use yii\helpers\ArrayHelper;
/**
 * This is the ActiveQuery class for [[Tiket]].
 *
 * @see Tiket
 */
class TiketQuery extends \yii\db\ActiveQuery {
	/*public function active()
	{
		return $this->andWhere('[[status]]=1');
	}*/
	/**
	 * {@inheritdoc}
	 * @return Tiket[]|array
	 */
	public function all( $db = null ) {
		return parent::all( $db );
	}
	/**
	 * {@inheritdoc}
	 * @return Tiket|array|null
	 */
	public function one( $db = null ) {
		return parent::one( $db );
	}
	public function getDateSoldOut( $jenisTour, $minDate, $maxDate ) {
		$times    = Tiket::hoursRange( Yii::$app->params[ 'timeslot' ][ 'lower' ] * 3600,
			Yii::$app->params[ 'timeslot' ][ 'upper' ] * 3600, Yii::$app->params[ 'timeslot' ][ 'step' ] * 60, 'H:i' );
		$disabled = Yii::$app->params[ 'timeslot' ][ 'disabled' ];
		$max      = sizeof( array_diff( $times, $disabled ) ) * Yii::$app->params[ 'timeslot' ][ 'max' ];
		return Yii::$app->db->createCommand( "SELECT tiket_tgl,SUM(jml) as totaljml 
			FROM tiket
			WHERE (jenis_tur = :jenis_tur AND tiket_tgl >= :minDate AND tiket_tgl <= :maxDate) 
			AND (expired > now() OR LENGTH(no_invoice) > 0)
			GROUP BY tiket_tgl
			HAVING totaljml >= :max", [
			':jenis_tur' => $jenisTour,
			':minDate'   => $minDate,
			':maxDate'   => $maxDate, ':max' => $max
		] )->queryColumn();
	}
	public function getTimeSlot( $jenisTour, $tiket_tgl ) {
		try {
			$result = Yii::$app->db->createCommand( "SELECT TIME_FORMAT(tiket_jam,'%H:%i') as tiket_jam,SUM(jml) as total
			FROM tiket
			WHERE (jenis_tur = :jenis_tur AND tiket_tgl = :tiket_tgl) 
			AND (expired > now() OR LENGTH(no_invoice) > 0)
			GROUP BY tiket_jam", [
				':jenis_tur' => $jenisTour,
				':tiket_tgl' => $tiket_tgl
			] )->queryAll();
			return ArrayHelper::map( $result, "tiket_jam", 'total' );
		} catch ( Exception $e ) {
			return [];
		}
	}
	public function getSisaTimeSlot( $jenisTour, $tiket_date, $tiket_time ) {
		try {
			$jml  = Yii::$app->db->createCommand( "SELECT COALESCE(sum(jml),0) as jml
			FROM tiket
			WHERE (jenis_tur = :jenis_tur AND tiket_tgl = :tiket_tgl AND TIME_FORMAT(tiket_jam,'%H:%i') = :tiket_time)
			AND (expired > now() OR LENGTH(no_invoice) > 0)", [
				':jenis_tur'  => $jenisTour,
				':tiket_tgl'  => $tiket_date,
				':tiket_time' => $tiket_time
			] )->queryScalar();
			$sisa = intval( Yii::$app->params[ 'timeslot' ][ 'max' ] ) - intval( $jml );
			if ( $sisa < 0 ) {
				$sisa = 0;
			}
			return $sisa;
		} catch ( Exception $e ) {
			return [];
		}
	}
}
